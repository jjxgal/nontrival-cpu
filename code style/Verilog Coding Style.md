# Verilog Coding Style

### version:1.1

### author: Jiaxi Jiang , YiTao Yang

本文档参照了https://verilogcodingstyle.readthedocs.io/en/latest/source/Preface.html

https://blog.csdn.net/k331922164/article/details/52166038/

## 1.命名规范

### 1.1 文件命名

+ 每一个.v或者.sv文件**只能包含一个**<u>module</u>或者<u>class</u>，<u>package</u>（这两者都是sv里面的，我暂时也不太明白）

+ 文件名需要和文件内内容名称相同。废话。

+ 如果是**头文件**的话，以.vh或者.svh为后缀。好吧这是废话。

+ 如果这个module或者class等属于一个文件夹A，那么这个module的名字需要以文件夹名A为前缀，以下划线连接。这个比较重要。

+ **后缀**可以用来描述module，比如top ,tb。

### 1.2 module、class、package、function、task命名

- 和信号命名方式一样采用**大驼峰**的格式命名，package以Pkg结尾，fuction,task这种也是。

- module的命名需要以它所从属的文件夹名为前缀，以下划线连接。之前说过。

  比如我现在有一个Comb文件夹，里面装着所有的组合电路，那么在这个文件夹里面的 译码器module的名字就可以是

  Comb_decoder（只是举个例子）

- module例化使用前缀U_xxx或者Ux_xxx。


### 1.3 信号命名

- 和模块命名相似，采用**前缀 + 大驼峰**的格式命名
- 除了采用**大驼峰命名法**之外，还需要**注意前缀**的使用（前缀和信号名中间**不需要**用下划线链接）

  **端口前缀信号**

  `i`作为前缀表示`input`.

  `o`作为前缀表示`output`.

  `b`作为前缀表示双向端口（应该没人这么写吧）

  `r`表示读。

  `w`表示写。

  `a`表示异步。

  `n`表示低有效。

  优先级如下

  i/o/b>a>n>r/w。

- **后缀的使用**

  后缀和信号名之间请用<u>**下划线**</u>链接。

  后缀表示信号属性发生的变化。

  比如

```systemverilog
assign data = ...;
always_ff (posedge clk) begin
  data_r <= data;
end//data_r表示保存了data的寄存器的输出
```

​		f表示寄存器下一个时钟周期的信号

```
logic [7:0] rWrData,rWrData_f;
assign rWrData_f = ... ;
always_ff (posedge clk) begin
  rWrData <= rWrData_f;
end
```

​		i表示取反  例如：`assign aaaBbbVld_i = ~aaaBbbVld;`

​		s表示同步后的信号，不懂。

### 1.4 重要：模块之间接口信号命名方式

  + 对于**CPU**与**其他模块之间**的接口命名方式（例如MMU CACHE等）
    + 分为**两个部分**  第一部分表明数据传输方向，其中**数据发出方在前**，**数据接收方在后**。
    + 第二个部分为正常的接口信号即**大驼峰命名法**。
    + 两部分以下划线链接。
      + 例：**CPUMMU_WrReq(CPU发送给MMU的写请求信号)**

  + 对于**流水线内部**的各个模块接口命名方式
    + 由两部分组成。第一部分为**流水线级数（IF/ID/EXE/MEM/WB）**
    + 第二部分为模块的接口信号，采用**大驼峰命名法**。
    + 两部分以下划线链接
      + 例：**EXE_AluOp(流水线EXE级中ALU的控制信号)**

### 1.5 参数和宏命名

+ 请**所有字母大写**

+ 单词中间请使用**下划线链接**。

+ 参数的定义请以P_打头。

## 2. 格式规范

### 2.1 文件头

+ 每个文件都要写文件头，端口处定义的参数和IO的完整注释要写在文件头中。因为每个文件只有一个模块，所以一个文件头里面只要写一个端口。

示例如下

```verilog
///////////////////////////////////////////////////////////////////////////////
// Copyright(C) Zion Team. Open source License: MIT.
// ALL RIGHT RESERVED
// File name   : Demo.sv
// Author      : Zion
// Date        : 2019-06-20
// Version     : 0.1
// Description :
//    ...
//    ...
// Parameter   :
//    ...
//    ...
// IO Port     :
//    ...
//    ...
// Modification History:
//   Date   |   Author   |   Version   |   Change Description
//==============================================================================
// 19-06-02 |    Zion    |     0.1     | Original Version
// ...
////////////////////////////////////////////////////////////////////////////////
```

### 2.2 代码格式

+ 代码缩进（因为我使用的vscode会自动代码缩进，所以我也不知道怎么描述，看着清晰点就好）语句最好对齐。
  + begin放在当前行的末尾，不要重新开启一行。
  + end 后面加上注释,表示是哪个关键词的结束。

![img](https://img-blog.csdn.net/20160809210918028)



endmodule endcase后面也注释一下end了哪个module

端口格式定义

```systemverilog
module DemoLib_ModuleXxxYyy // 单独一行，前后无空格。
import DemoAaaPkg::*;       // 引用package，单独一行，前后无空格。
import DemoBbbPkg::*;       // 多个package写在不同的行中。
#(P_A = "_",                // 第一个参数以 '#(' 开头，定义在新行中，前后无空格，省略parameter标识符。
  P_B = "_",                // 其他parameter在新的行中定义，定义前需要 2个空格 进行缩进。
localparam                  // 若存在local parameter，localparam在新的一行中定义，前后无空格。
  P_B_LG = $clog2(P_B),     // local parameter定义格式与parameter相同。
  P_C = P_A - 1
)(                          // 在新的行中写 '参数定义右括号' 和 '端口定义左括号'。
  input        clk,rst ,    // 端口在新行中定义，2个空格缩进。'clk,rst' 可以写在同一行。
  input        iEn     ,    // 端口定义顺序：input, inout, output。
  input        iDat    ,    // 同方向端口定义顺序：clock, reset, enable, data。
  inout        bVld    ,    // 端口和参数定义结尾的逗号分隔符可以对齐也可以不对齐。
  output logic oDat         // 代码中端口部分参数和信号后可添加简要注释，完整注释在文件头中添加。
);                          // 端口定义 右括号 及 分号 单独一行，前后无空格。
  ...
endmodule // DemoLib_ModuleXxxYyy //单独一行，前后无空格。添加 ending name。 


module DemoLib_Aaa
(                           // 如果没有模块中没有参数，直接在新行中写接口定义左括号。
  input  clk,rst,
  input  iDat,
  output oDat
);
  ...
endmodule // DemoLib_Aaa
```

如果模块端口较多，且不同端口连接模块不同，可以按照连接关系对端口进行分组：

```systemverilog
module DemoGroupIO
(
  // function A IO
  input  a1,
  input  a2,
  output a3,
  // function B IO
  input  b1,
  output b2,
  // function C IO
  input c1,
  input c2
);
  ......
endmodule // DemoGroupIO
```

### 2.3 module的例化格式

模块例化时，参数在例化时通过 **‘#()’** 直接传递，例如

```
Adder
  U_Adder(
    .a(a),
    .b(b),
    .o(o)
  );

Sub #(
    .type_A(logic [3:0]),
    .type_B(logic [3:0]))
  U_Sub(
    .a(a),
    .b(b),
    .o(o)
  );

And #(.width(8)) U_And(.a(a),.b(b),.o(o));
```

- 使用 **‘.port(signal)’** 连接信号和端口。
- 不得使用顺序连接的方式连接端口。

### 2.4 使用Typedef 自定义变量 

使用Typedef自定义变量，变量名要使用Type结尾。

比如(这是清华的代码)

```systemverilog
typedef struct packed {
	// ar
	logic        arready;
	// r
	logic [31:0] rdata;
	logic [1 :0] rresp;
	logic        rlast;
	logic        rvalid;
	// aw
	logic        awready;
	// w
	logic        wready;
	// b
	logic [1 :0] bresp;
	logic        bvalid;
} axi_respType;
```

### 2.5 定义interface和modport时

变量名后面以Interface和Modport结尾

