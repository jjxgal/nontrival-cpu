# 说明
本工程为西北工业大学计算机学院组成原理试点班的大作业。\
实现了mips的经典五级流水线，50多条指令，sram总线，有独立的乘法器与除法器，通过了龙芯89个功能测试点，可以对操作系统提供支持。\
本项目的作者也因此拿到了计算机组成原理与系统结构理论与实验课程的满分。


## 测试说明
本项目是一个vivado的工程。
其中func_test_v0.01为测试需要的vivado工程文件。\
myCPU为我设计的cpu的verilog源代码。\
用vivado打开功能测试工程，然后把myCPU中的测试文件加入便可以开始测试。

我设计cpu的top接口如下所示（即为vivado工程所要求的接口）：

```
mycpu_top cpu(
    .clk              (cpu_clk   ),
    .resetn           (cpu_resetn),  //low active
    .int              (6'd0      ),  //interrupt,high active

    .inst_sram_en     (cpu_inst_en   ),
    .inst_sram_wen    (cpu_inst_wen  ),
    .inst_sram_addr   (cpu_inst_addr ),
    .inst_sram_wdata  (cpu_inst_wdata),
    .inst_sram_rdata  (cpu_inst_rdata),
    
    .data_sram_en     (cpu_data_en   ),
    .data_sram_wen    (cpu_data_wen  ),
    .data_sram_addr   (cpu_data_addr ),
    .data_sram_wdata  (cpu_data_wdata),
    .data_sram_rdata  (cpu_data_rdata),

    //debug
    .debug_wb_pc      (debug_wb_pc      ),
    .debug_wb_rf_wen  (debug_wb_rf_wen  ),
    .debug_wb_rf_wnum (debug_wb_rf_wnum ),
    .debug_wb_rf_wdata(debug_wb_rf_wdata)
); 
```
张舵 2019.7.7