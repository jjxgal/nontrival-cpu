module wb(
    MEM_WB_WBSel,MEM_WB_ALUOut,MEM_WB_dm_dout,MEM_WB_PC,
    WBData
);
    input [1:0] MEM_WB_WBSel;
    input [31:0] MEM_WB_ALUOut;
    input [31:0] MEM_WB_dm_dout;
    input [31:0] MEM_WB_PC;

    output reg [31:0] WBData;

    always @ (MEM_WB_WBSel or MEM_WB_ALUOut or MEM_WB_dm_dout or MEM_WB_PC) begin
        if (MEM_WB_WBSel == 2'b00)
            WBData <= MEM_WB_dm_dout;
        else if (MEM_WB_WBSel == 2'b01)
            WBData <= MEM_WB_ALUOut;
        else if (MEM_WB_WBSel == 2'b10)
            WBData <= MEM_WB_PC + 8;
        else
            WBData <= MEM_WB_ALUOut;
    end
endmodule 