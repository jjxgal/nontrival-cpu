
module mul_fsm(
                multiplicand,multiplier,clock,rst,start,

                busy,product,HLWr
            );
    
    input [31:0] multiplier;
    input [31:0] multiplicand;
    input clock;
    input rst;
    input start;
    
    
    output reg busy;
    output [63:0] product;
    output reg HLWr;

    // Initialising required registers
    reg [64:0] reg_multiplicand;
    reg [95:0] reg_product;
    reg [5:0] count;



    // Initialising required wires
    wire [63:0] add = reg_product[95:32] + reg_multiplicand;
    wire [63:0] product_high = reg_product[0] ? add : reg_product[95:32]; 
    wire [95:0] product_all = {product_high,reg_product[31:0]} >> 1'b1;


    // assigning outputs
    assign product = product_all[63:0];

    


    // reg_multiplicand
    always @ (posedge clock) begin
        if (rst)
            reg_multiplicand <= 0;
        else if (start)
            reg_multiplicand <= {32'b0,multiplicand};
        else
            reg_multiplicand <= reg_multiplicand;
     
    end


    // reg_product
    always @ (posedge clock) begin
        if (rst)
            reg_product <= 96'b0;
        else if (start)
            reg_product <= {64'b0,multiplier};
        else if (count < 6'b100000)
            reg_product <= product_all;
        else
            reg_product <= 96'b0;
    end

    // busy
    always @ (rst or count) begin
        if (rst)
            busy = 1'b0;
        else if (count > 6'b011111)
            busy = 1'b0;
        else if (count == 6'b0)
            busy = 1'b0;
        else
            busy = 1;
    end
        
    
    // count
    always @ (posedge clock) begin
        if (rst || count > 6'b011111)
            count <= 6'b0;
        else if(count < 6'b100000 && (start || busy))
            count <= count + 6'b000001;
        
    end
    
    // HLWr
    always @ (count) begin
        if (count == 6'b100000)
            HLWr = 1;
        else
            HLWr = 0;
    end

endmodule