module id(
            clk,rst,
            IF_ID_ins,stall,
            Bran_ASel,Bran_BSel,EX_MEM_ALUOut,
            WBData,MEM_WB_Rd,MEM_WB_RFWr,all_flush,

            rs,rt,rd,Dec_shift,EXTRes,
            Dec_ALUOp,NPCOp,
            Dec_DMWr,Dec_RFWr,PCWr,IF_IDWr,
            Dec_DMRe,Dec_WBSel,Dec_RSel,IF_flush,
            busA,busB,SWData,Dec_BSel,sign,shift_var,
            Dec_MEMOp,Dec_In_A,Dec_start,Dec_HLOp,
            Dec_Exc_Kind,Dec_CP0Re,Dec_CP0Wr,Dec_nop,overflow
        );

    input clk;
    input rst;

    input [31:0] IF_ID_ins;
    input stall;

    //forwarding for branch
    input [1:0] Bran_ASel;
    input [1:0] Bran_BSel;
    input [31:0] EX_MEM_ALUOut;
    input [31:0] WBData;

    input [4:0] MEM_WB_Rd;
    input MEM_WB_RFWr;
    input all_flush;
    

    output [4:0] rs;
    output [4:0] rt;
    output [4:0] rd;
    output [4:0] Dec_shift;
    


    output reg [3:0] Dec_ALUOp;
    output reg [1:0] NPCOp;

    output reg Dec_DMWr;
    output reg Dec_RFWr;
    output reg PCWr;
    output reg IF_IDWr;
    output reg Dec_DMRe;
    output reg [1:0] Dec_WBSel;
    output reg [1:0] Dec_RSel;
    output reg [31:0] EXTRes;

    
    output reg IF_flush; 

    output reg [31:0] busA;
    output reg [31:0] busB;
    
    output [31:0] SWData;
    output Dec_BSel;
    output sign;
    output overflow;
    output shift_var;

    output reg [2:0] Dec_MEMOp;
    output reg [31:0] Dec_In_A;
    
    output reg Dec_start;
    output reg [2:0] Dec_HLOp;

    output reg [2:0] Dec_Exc_Kind;
    output reg Dec_CP0Wr;
    output reg Dec_CP0Re;
    output reg Dec_nop;
    
    reg reserved_ins;
    reg EXTOp;
    
    reg Dec_BSel;
    reg in_slot;

    wire [5:0] op;
    wire [5:0] func;

// arithmetic instruction
    wire add;
    wire sub;
    wire addi;
    wire addiu;
    wire slt;
    wire slti;
    wire sltu;
    wire sltiu;

    wire multu;
    wire mult;
    wire div;
    wire divu;
// arithmetic instruction end

// logic instruction
    wire andins;
    wire andi;
    wire lui;
    wire Nor;
    wire Or;
    wire Xor;
    wire xori;
// logic instruction end

// shift instruction
    wire sllv;
    wire sll;
    wire srav;
    wire sra;
    wire srlv;
    wire srl;
// shift instruction end

// branch and jump instruction
    wire bne;
    wire bgez;
    wire bgtz;
    wire blez;
    wire bltz;
    wire bgezal;
    wire bltzal;

    wire j;
    wire jr;
    wire jalr;
// branch and jump instruction end

// memory instruction
    
    wire lb;
    wire lbu;
    wire lh;
    wire lhu;
    wire sb;
    wire sh;

// memory instruction end

// data movement instruction
    wire mfhi;
    wire mflo;
    wire mthi;
    wire mtlo;
// data movement instruction end

// interrupt instruction
    
    wire break;
    wire syscall;
    wire eret;
    wire mfc0;
    wire mtc0;

// interrupt instruction end

// basic 7 ins
    wire addu;
    wire subu;
    wire ori;
    wire lw;
    wire sw;
    wire beq;
    wire jal;
    wire nop;
// basic 7 ins end
    
    wire RFWr_ins;

    wire ALU_add;
    wire ALU_sub;
    wire ALU_Less;
    wire ALU_and;
    wire ALU_or;
    wire ALU_nor;
    wire ALU_xor;
    wire ALU_lui;
    wire ALU_left_shift;
    wire ALU_right_logic_shift;
    wire ALU_right_arith_shift;

    wire R_Type;
    wire I_Type;
    wire Sign_Extend;
    wire jump;

    wire judge_slot;
    

    assign op = IF_ID_ins[31:26];
    assign func = IF_ID_ins[5:0];
    assign Dec_shift = IF_ID_ins[10:6];
    assign rs = IF_ID_ins[25:21];
    assign rt = IF_ID_ins[20:16];
    assign rd = IF_ID_ins[15:11];


// arithmetic instruction
    assign add = (op == 6'b000000 && func == 6'b100000) ? 1 : 0;
    assign sub = (op == 6'b000000 && func == 6'b100010) ? 1 : 0;
    assign addi = (op == 6'b001000) ? 1 : 0;
    assign addiu = (op == 6'b001001) ? 1 : 0;
    assign slt = (op == 6'b000000 && func == 6'b101010) ? 1 : 0;
    assign slti = (op == 6'b001010) ? 1 : 0;
    assign sltu = (op == 6'b000000 && func == 6'b101011) ? 1 : 0;
    assign sltiu = (op == 6'b001011) ? 1 : 0;

    assign multu = (op == 6'b000000 && func == 6'b011001) ? 1 : 0;
    assign mult = (op == 6'b000000 && func == 6'b011000) ? 1 : 0;
    assign div = (op == 6'b000000 && func == 6'b011010) ? 1 : 0;
    assign divu = (op == 6'b000000 && func == 6'b011011) ? 1 : 0;
// arithmetic instruction end

// logic instruction

    assign andins = (op == 6'b000000 && func == 6'b100100) ? 1 : 0;
    assign andi = (op == 6'b001100) ? 1 : 0;
    assign lui = (op == 6'b001111) ? 1 : 0;
    assign Nor = (op == 6'b000000 && func == 6'b100111) ? 1 : 0;
    assign Or = (op == 6'b000000 && func == 6'b100101) ? 1 : 0;
    assign Xor = (op == 6'b000000 && func == 6'b100110) ? 1 : 0;
    assign xori = (op == 6'b001110) ? 1 : 0;

// logic instruction end

// shift instruction

    assign sllv = (op == 6'b000000 && func == 6'b000100);
    assign sll = (op == 6'b000000 && func == 6'b000000);
    assign srav = (op == 6'b000000 && func == 6'b000111);
    assign sra = (op == 6'b000000 && func == 6'b000011);
    assign srlv = (op == 6'b000000 && func == 6'b000110);
    assign srl = (op == 6'b000000 && func == 6'b000010);

// shift instruction end

// branch and jump instruction

    assign bne = (op == 6'b000101) ? 1 : 0;
    assign bgez = (op == 6'b000001 && IF_ID_ins[20:16] == 5'b00001) ? 1 : 0;
    assign bgtz = (op == 6'b000111 && IF_ID_ins[20:16] == 5'b00000) ? 1 : 0;
    assign blez = (op == 6'b000110 && IF_ID_ins[20:16] == 5'b00000) ? 1 : 0;
    assign bltz = (op == 6'b000001 && IF_ID_ins[20:16] == 5'b00000) ? 1 : 0;
    assign bgezal = (op == 6'b000001 && IF_ID_ins[20:16] == 5'b10001) ? 1 : 0;
    assign bltzal = (op == 6'b000001 && IF_ID_ins[20:16] == 5'b10000) ? 1 : 0;
    assign j = (op == 6'b000010) ? 1 : 0;
    assign jr = (op == 6'b000000 && func == 6'b001000) ? 1 : 0;
    assign jalr = (op == 6'b000000 && func == 6'b001001) ? 1 : 0;

// branch and jump instruction end

// memory instruction

    assign lb = (op == 6'b100000) ? 1 : 0;
    assign lbu = (op == 6'b100100) ? 1 : 0;
    assign lh = (op == 6'b100001) ? 1: 0;
    assign lhu = (op == 6'b100101) ? 1 : 0;
    assign sb = (op == 6'b101000) ? 1 : 0;
    assign sh = (op == 6'b101001) ? 1 : 0;

// memory instruction end

// data movement instruction

    assign mfhi = (op == 6'b000000 && func == 6'b010000) ? 1 : 0;
    assign mflo = (op == 6'b000000 && func == 6'b010010) ? 1 : 0;
    assign mthi = (op == 6'b000000 && func == 6'b010001) ? 1 : 0;
    assign mtlo = (op == 6'b000000 && func == 6'b010011) ? 1 : 0;

// data movement instruction end

//// interrupt instruction
    
    assign break = (op == 6'b000000 && func == 6'b001101) ? 1 : 0;
    assign syscall = (op == 6'b000000 && func == 6'b001100) ? 1 : 0;
    assign eret = (IF_ID_ins[31:25] == 7'b0100001) ? 1 : 0;
    assign mfc0 = (IF_ID_ins[31:21] == 11'b01000000000) ? 1 : 0;
    assign mtc0 = (IF_ID_ins[31:21] == 11'b01000000100) ? 1 : 0;

// interrupt instruction end

// basic 7 instruction
    assign addu = (func == 6'b100001 && op == 6'b000000) ? 1 : 0;
    assign subu = (func == 6'b100011 && op == 6'b000000) ? 1 : 0;
    assign ori = (op == 6'b001101) ? 1 : 0;
    assign lw = (op == 6'b100011) ? 1 : 0;
    assign sw = (op == 6'b101011) ? 1 : 0;
    assign beq = (op == 6'b000100) ? 1 : 0;
    assign jal = (op == 6'b000011) ? 1 : 0;
    assign nop = (IF_ID_ins[31:0] == 32'b0) ? 1 : 0;
// basic 7 instruction


    assign RFWr_ins = (addu || subu || lw || ori || jal || add || sub || addi || addiu
                        || slt || slti || sltiu || sltu || andins || andi || lui || Nor 
                        || Or || Xor || xori || sllv || sll || srav || sra || srlv || srl
                        || bgezal || bltzal || jalr || lb || lbu || lh || lhu 
                        || mfhi || mflo || mfc0) ? 1 : 0;

    assign ALU_add = (addu || lw || sw || add || addi || addiu 
                    || lb || lbu || lh || lhu || sb || sh) ? 1 : 0;
    assign ALU_sub = (subu || sub) ? 1 : 0;
    assign ALU_Less = (slt || slti || sltiu || sltu) ? 1 : 0;
    assign ALU_and = (andins || andi) ? 1 : 0;
    assign ALU_nor = (Nor) ? 1 : 0;
    assign ALU_or = (Or || ori) ? 1 : 0;
    assign ALU_xor = (Xor || xori) ? 1 : 0;
    assign ALU_lui = (lui) ? 1 : 0;
    assign ALU_left_shift = (sllv || sll) ? 1 : 0;
    assign ALU_right_logic_shift = (srlv || srl) ? 1 : 0;
    assign ALU_right_arith_shift = (srav || sra) ? 1 : 0;


    assign R_Type = (addu || subu || add || sub || slt || sltu 
                    || andins || Nor || Or || Xor || sllv || sll 
                    || srav || sra || srlv || srl || multu || mult || div || divu) ? 1 : 0;

    assign I_Type = (ori || addi || addiu || lw || sw || slti || sltiu || andi || xori || lui 
                    || lb || lbu || lh || lhu || sb || sh) ? 1 : 0;

    assign sign = (add || sub || addi || slt || slti || mult || div) ? 1 : 0;

    assign overflow = (add || sub || addi)  ? ~stall : 0;

    assign Sign_Extend = (lw || sw || addi || addiu || slti || sltiu 
                        || lb || lbu || lh || lhu || sb || sh) ? 1 : 0;


    assign jump = (j || jr || jal || jalr) ? 1 : 0;
    assign shift_var = (sllv || srav || srlv) ? 1 : 0;
    assign judge_slot = (j || jr || jal || jalr || beq || bne || bgtz || blez || bgez
                        || bltz || bgezal || bltzal) ? 1 : 0;

// rf
    wire [4:0] Ra;
    wire [4:0] Rb;
    wire [4:0] Rw;
    wire RFWr;
    wire [1:0] BSel;
    wire [31:0] busW;

    reg [31:0] rf [31:0];

    assign Ra = rs;
    assign Rb = rt;
    assign Rw = MEM_WB_Rd;
    assign RFWr = MEM_WB_RFWr;
    assign busW = WBData;
    assign BSel = Dec_BSel;

    integer i;
    // always @ (posedge rst) begin
    //     if (rst) begin
    //         for (i = 0;i < 32;i = i + 1)
    //             rf[i] = 32'b0;
    //     end
    // end

    // rf[Rw]
    always @ (posedge clk) begin
        if (rst) begin
            for (i = 0;i < 32;i = i + 1)
                rf[i] = 32'b0;
        end
        else if (Rw == 0)
            rf[Rw] <= 32'b0;
        else if (RFWr)
            rf[Rw] <= busW;
    end

    // busA
    always @ (Ra or Rw or busW or rf[Ra] or RFWr) begin
        if (Rw == Ra && RFWr)
            busA = busW;
        else
            busA = rf[Ra];
    end

    // busB
    always @ (rf[Rb] or Rw or busW or Rb or RFWr) begin
        if (Rb == Rw && RFWr)
            busB = busW;
        else
            busB = rf[Rb];
    end

    // SWData
    assign SWData = busB;

//rf end

    

    // ALUOp
    always @ (*) begin
        if (stall)
            Dec_ALUOp = 4'b0000;
        else if (ALU_add) // plus
            Dec_ALUOp = 4'b0000; 
        else if (ALU_sub)   // sub
            Dec_ALUOp = 4'b0001;
        else if (ALU_or)
            Dec_ALUOp = 4'b0010;
        else if (ALU_Less)
            Dec_ALUOp = 4'b0011;
        else if (ALU_and)
            Dec_ALUOp = 4'b0100;
        else if (ALU_nor)
            Dec_ALUOp = 4'b0101;
        else if (ALU_xor)
            Dec_ALUOp = 4'b0110;
        else if (ALU_lui)
            Dec_ALUOp = 4'b0111;
        else if (ALU_left_shift)
            Dec_ALUOp = 4'b1000;
        else if (ALU_right_logic_shift)
            Dec_ALUOp = 4'b1001;
        else if (ALU_right_arith_shift)
            Dec_ALUOp = 4'b1010;
        else if (multu || mult)
            Dec_ALUOp = 4'b1011;
        else if (div || divu)
            Dec_ALUOp = 4'b1100;
        else 
            Dec_ALUOp = 4'b0000;
    end

    // DMWr
    always @ (sw or sb or sh or nop or stall or all_flush) begin
        if ((sw || sb || sh)  && !nop && !stall && !all_flush)
            Dec_DMWr = 1'b1;
        else
            Dec_DMWr = 1'b0;
    end

    // RFWr
    always @ (RFWr_ins or nop or stall or all_flush) begin
        if (RFWr_ins && !nop && !stall && !all_flush)
            Dec_RFWr = 1'b1;
        else
            Dec_RFWr = 1'b0;
    end

    // PCWr
    always @ (stall) begin
        if (stall)
            PCWr = 0;
        else
            PCWr = 1;
    end

    // IF_IDWr
    always @ (stall) begin
        if (stall)
            IF_IDWr = 0;
        else
            IF_IDWr = 1;
    end

    // DMRe
    always @ (*) begin
        if (stall)
            Dec_DMRe = 1'b0;
        else if (lw || lb || lbu || lh || lhu)
            Dec_DMRe = 1'b1;
        else
            Dec_DMRe = 1'b0;
    end

    // EXTOp
    always @ (Sign_Extend) begin
        if (Sign_Extend)
            EXTOp = 1;
        else
            EXTOp = 0;
    end

    // WBSel
    always @ (*) begin
        if (jal || jalr || bgezal || bltzal)
            Dec_WBSel = 2'b10;
        else if (lw || lb || lbu || lh || lhu)
            Dec_WBSel = 2'b00;
        else
            Dec_WBSel = 2'b01; 
    end

    // BSel
    always @ (R_Type) begin
        if (R_Type)
            Dec_BSel = 1;
        else 
            Dec_BSel = 0;
    end

    // RSel
    always @ (*) begin
        if (jal || jalr || bgezal || bltzal)
            Dec_RSel = 2'b00;
        else if (R_Type || mfhi || mflo)
            Dec_RSel = 2'b01;
        else
            Dec_RSel = 2'b10;
    end
    
    // MEMOp
    always @ (*) begin
        if (stall)
            Dec_MEMOp = 3'b111;
        else if (lw || sw)
            Dec_MEMOp = 3'b000;
        else if (lh || sh)
            Dec_MEMOp = 3'b001;
        else if (lb || sb)
            Dec_MEMOp = 3'b010;
        else if (lbu)
            Dec_MEMOp = 3'b011;
        else if (lhu)
            Dec_MEMOp = 3'b100;
        else
            Dec_MEMOp = 3'b111;
    end

    // Dec_start
    always @ (*) begin
        if ((multu || mult || div || divu )&& !stall && !nop && !all_flush)
            Dec_start = 1;
        else
            Dec_start = 0;
    end

    // HLOp
    always @ (mfhi or mflo or mthi or mtlo or stall or nop or all_flush) begin
        if (mfhi && !stall && !nop && !all_flush)
            Dec_HLOp = 3'b000;
        else if (mflo && !stall && !nop && !all_flush)
            Dec_HLOp = 3'b001;
        else if (mthi && !stall && !nop && !all_flush)
            Dec_HLOp = 3'b010;
        else if (mtlo && !stall && !nop && !all_flush)
            Dec_HLOp = 3'b011;
        else
            Dec_HLOp = 3'b111;
    end

    // Dec_Exc_Kind
    always @ (syscall or eret or break or stall or reserved_ins) begin
        if (syscall && !stall)
            Dec_Exc_Kind = 3'b001;
        else if (break && !stall)
            Dec_Exc_Kind = 3'b010;
        else if (eret && !stall)
            Dec_Exc_Kind = 3'b011;
        else if (reserved_ins)
            Dec_Exc_Kind = 3'b100;
        else 
            Dec_Exc_Kind = 3'b000;
    end

    // Dec_CP0Wr
    always @ (mtc0 or stall or all_flush or nop) begin
        if (mtc0 && !stall && !all_flush && !nop)
            Dec_CP0Wr = 1;
        else
            Dec_CP0Wr = 0;
    end

    // Dec_CP0Re
    always @ (mfc0) begin
        if (mfc0)
            Dec_CP0Re = 1;
        else
            Dec_CP0Re = 0;
    end


    // Dec_nop
    always @ (in_slot) begin
        if (in_slot)
            Dec_nop = 1;
        else
            Dec_nop = 0;
    end

    // reserved_ins
    always @ (*) begin
        if (add | sub | addi | addiu | slt | slti | sltu | sltiu
            | multu | mult | div | divu | andins | andi | lui | Nor
            | Or | Xor | xori | sllv | sll | srav | sra | srlv | srl
            | bne | bgez | bgtz | blez | bltz | bgezal | bltzal | j
            | jr | jalr | lb | lbu | lh | lhu | sb | sh | mfhi | mflo
            | mthi | mtlo | break | syscall | eret | mfc0 | mtc0 
            | addu | subu | ori | lw | sw | beq | jal | nop)
            reserved_ins = 0;
        else
            reserved_ins = 1;
    end
    
    // in_slot
    always @ (posedge clk) begin
        if (judge_slot)
            in_slot <= 1;
        else
            in_slot <= 0;
    end
    

// EXT
    always @ (EXTOp or IF_ID_ins) begin
        if (EXTOp == 1'b1)
            EXTRes <= {{16{IF_ID_ins[15]}},IF_ID_ins[15:0]};
        else
            EXTRes <= {16'b0,IF_ID_ins[15:0]};
    end
//


// NPC
// This part would deal with the branch ins and j ins
    

    reg branch;
    reg [31:0] In_A;
    reg [31:0] In_B;
    // Dec_In_A
    always @ (In_A)
        Dec_In_A = In_A;

    // In_A
    always @ (Bran_ASel or busA or EX_MEM_ALUOut or WBData)
        if (Bran_ASel == 2'b00)
            In_A = busA;
        else if (Bran_ASel == 2'b01)
            In_A = EX_MEM_ALUOut;
        else if (Bran_ASel == 2'b10)
            In_A = WBData;
        else
            In_A = 32'b0;

    // In_B
    always @ (Bran_BSel or EX_MEM_ALUOut or WBData or busB)
        if (Bran_BSel == 2'b00)
            In_B = busB;
        else if (Bran_BSel == 2'b01)
            In_B = EX_MEM_ALUOut;
        else if (Bran_BSel == 2'b10)
            In_B = WBData;
        else
            In_B = 32'b0;

    // branch decide
    always @ (In_A or In_B or beq or bne or bgez or bgtz or blez or bltz or bgezal or bltzal) begin
        if (In_A == In_B && beq)
            branch = 1;
        else if (In_A != In_B && bne)
            branch = 1;
        else if ($signed(In_A) >= 0 && bgez)
            branch = 1;
        else if ($signed(In_A) > 0 && bgtz)
            branch = 1;
        else if ($signed(In_A) <= 0 && blez)
            branch = 1;
        else if ($signed(In_A) < 0 && bltz)
            branch = 1;
        else if ($signed(In_A) >= 0 && bgezal)
            branch = 1;
        else if ($signed(In_A) < 0 && bltzal)
            branch = 1;
        else
            branch = 0;
    end

    // NPCOp
    always @ (*) begin
        if (j || jal)
            NPCOp = 2'b10;
        else if (branch)
            NPCOp = 2'b01;
        else if (jr || jalr)
            NPCOp = 2'b11;
        else
            NPCOp = 2'b00;
    end

    // If_flush
    always @ (branch or jump or stall or all_flush) begin
        if (all_flush)
            IF_flush = 1;
        else if ((jump || branch) && !stall)
            IF_flush = 0;
        else 
            IF_flush = 0;
    end

// NPC end




endmodule