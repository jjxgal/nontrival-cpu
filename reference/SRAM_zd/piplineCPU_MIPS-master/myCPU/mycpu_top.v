module mycpu_top( 
         clk, resetn, int, 

         inst_sram_rdata,
         data_sram_rdata,

         inst_sram_en,
         inst_sram_wen,
         inst_sram_addr,
         inst_sram_wdata,
         

         data_sram_en,
         data_sram_wen,
         data_sram_addr,
         data_sram_wdata,

         debug_wb_pc,
         debug_wb_rf_wen,
         debug_wb_rf_wnum,
         debug_wb_rf_wdata
         
   );

   input   clk;
   input   resetn;
   input [5:0] int;
   input [31:0] inst_sram_rdata;
   input [31:0] data_sram_rdata;

   output inst_sram_en;
   output [3:0] inst_sram_wen;
   output [31:0] inst_sram_addr;
   output [31:0] inst_sram_wdata;

   output data_sram_en;
   output [3:0] data_sram_wen;
   output [31:0] data_sram_addr;
   output [31:0] data_sram_wdata;

   output [31:0] debug_wb_pc;
   output [31:0] debug_wb_rf_wdata;
   output [3:0] debug_wb_rf_wen;
   output [4:0] debug_wb_rf_wnum;

   wire rst = ~resetn;
   
   

// IF
   
   wire [31:0] PC;
   wire [31:0] phy_PC;
   wire [31:0] im_dout;
   wire ft_exception;
// IF end


// IF_ID
   wire [31:0] IF_ID_PC;
   wire [31:0] IF_ID_ins;
   wire IF_ID_ft_exception;
// IF_ID


// ID
   wire [4:0] rs;
   wire [4:0] rt;
   wire [4:0] rd;
   wire [4:0] Dec_shift;
   wire [31:0] EXTRes;

   wire [3:0] Dec_ALUOp;
   wire [1:0] NPCOp;
   wire Dec_DMWr;
   wire Dec_RFWr;
   wire PCWr;
   wire IF_IDWr;
   wire Dec_DMRe;
   
   wire [1:0] Dec_WBSel;
   wire [1:0] Dec_RSel;
   
   wire IF_flush;

   wire [31:0] busA;
   wire [31:0] busB;
   wire Dec_BSel;

   wire [31:0] SWData;
   wire sign;
   wire shift_var;

   wire [2:0] Dec_MEMOp;
   wire [31:0] Dec_In_A;

   wire Dec_start;
   wire [2:0] Dec_HLOp;

   wire [2:0] Dec_Exc_Kind;
   wire Dec_CP0Re;
   wire Dec_CP0Wr;

   wire Dec_nop;

   wire overflow;
// ID end
// ID_EX
   wire ID_EX_RFWr;
   wire ID_EX_DMWr;
   wire ID_EX_DMRe;
   wire [1:0] ID_EX_WBSel;
   wire [1:0] ID_EX_RSel;
   wire [4:0] ID_EX_shift;
   wire [31:0] ID_EX_busA;
   wire [31:0] ID_EX_busB;
   wire [31:0] ID_EX_SWData;
   wire [4:0] ID_EX_rs;
   wire [4:0] ID_EX_rt;
   wire [4:0] ID_EX_rd;
   wire [3:0] ID_EX_ALUOp;
   wire [31:0] ID_EX_PC;
   wire [31:0] ID_EX_EXTRes;
   wire ID_EX_BSel;
   wire ID_EX_sign;
   wire ID_EX_shift_var;
   wire [2:0] ID_EX_MEMOp;
   wire ID_EX_start;
   wire [2:0] ID_EX_HLOp;
   wire [2:0] ID_EX_Exc_kind;
   wire ID_EX_CP0Re;
   wire ID_EX_CP0Wr;
   wire ID_EX_nop;
   wire ID_EX_overflow;
   wire ID_EX_ft_exception;
// ID_EX end

// EX
   wire [4:0] EX_Rd;
   wire [31:0] EX_SWData;
   wire [31:0] ALUOut;
   wire IntegerOverflow;

   wire [31:0] EX_High;
   wire [31:0] EX_Low;
   wire busy;
   wire Hi_HLWr;
   wire Lo_HLWr;
// EX end

// EX_MEM
   wire EX_MEM_RFWr;
   wire [1:0] EX_MEM_WBSel;
   wire [31:0] EX_MEM_ALUOut;
   wire [31:0] EX_MEM_SWData;
   wire [4:0] EX_MEM_Rd;
   wire EX_MEM_DMWr;
   wire [31:0] EX_MEM_PC;
   wire EX_MEM_DMRe;
   wire [2:0] EX_MEM_MEMOp;

   wire [31:0] High;
   wire [31:0] Low;

   wire [2:0] EX_MEM_Exc_kind;
   wire EX_MEM_CP0Wr;
   wire EX_MEM_CP0Re;
   wire EX_MEM_IntegerOverflow;
   wire EX_MEM_nop;
   wire [4:0] EX_MEM_really_Rd;
   wire EX_MEM_ft_exception;
// EX_MEM end

// MEM
   wire [31:0] dm_dout;
   wire AddressException;
   wire [3:0] rf_wen;
   wire [31:0] physical_address;
   wire [31:0] MEM_SWData;
// MEM end

// Exception
   wire [31:0] Exc_out;
   wire [1:0] EPCOp;
   wire all_flush;
   wire [31:0] EPC;
// Exception end

// MEM_WB
   wire [1:0] MEM_WB_WBSel;
   wire MEM_WB_RFWr;
   wire [31:0] MEM_WB_ALUOut;
   wire [31:0] MEM_WB_dm_dout; 
   wire [4:0] MEM_WB_Rd;
   wire [31:0] MEM_WB_PC;
   wire [3:0] MEM_WB_rf_wen;
   wire [2:0] MEM_WB_MEMOp;
   wire [31:0] WB_address;
// MEM_WB end

// // WB
   wire [31:0] WBData;
// // WB end

// // forwarding
   wire [1:0] ALU_ASel;
   wire [1:0] ALU_BSel;
   wire [1:0] Bran_ASel;
   wire [1:0] Bran_BSel;
   wire stall;
// forwarding end

// if
   pc U_PC (
      .clk(clk),.rst(rst),.PCWr(PCWr),.NPCOp(NPCOp),
      .IF_ID_PC(IF_ID_PC),.IF_ID_ins(IF_ID_ins),.Dec_In_A(Dec_In_A),
      .EPCOp(EPCOp),.EPC(EPC),

      .PC(PC),.phy_PC(phy_PC),.ft_exception(ft_exception)
   );

   // im_4k U_IM ( 
   //    .addr(PC[11:2]) , .dout(im_dout)
   // );
   // im_interface U_vim (
   //    .inst_sram_rdata(inst_sram_rdata),.PC(PC),

   //    .im_dout(im_dout),.inst_sram_addr(inst_sram_addr),
   //    .inst_sram_en(inst_sram_en),.inst_sram_wen(inst_sram_wen),
   //    .inst_sram_wdata(inst_sram_wdata)
   // );
   assign im_dout = inst_sram_rdata;
   assign inst_sram_addr = phy_PC;


   assign inst_sram_en = resetn ? !stall : 0;
      
   assign inst_sram_wen = 4'b0000;
   assign inst_sram_wdata = 32'b0;
   
// IF end
//IF_ID
   
   IF_ID U_IF_ID (
      .clk(clk),.rst(rst),.IF_IDWr(IF_IDWr),.im_dout(im_dout),
      .IF_flush(IF_flush),.all_flush(all_flush),
      .ft_exception(ft_exception),.stall(stall),

      .PC(PC),.IF_ID_PC(IF_ID_PC),.IF_ID_ins(IF_ID_ins),.IF_ID_ft_exception(IF_ID_ft_exception)
   );


// IF_ID end


// ID
   id U_ID (
      .clk(clk),.rst(rst),
      .IF_ID_ins(IF_ID_ins),.stall(stall),
      .Bran_ASel(Bran_ASel),.Bran_BSel(Bran_BSel),.EX_MEM_ALUOut(EX_MEM_ALUOut),
      .WBData(WBData),.MEM_WB_Rd(MEM_WB_Rd),.MEM_WB_RFWr(MEM_WB_RFWr),.all_flush(all_flush),

      .rs(rs),.rt(rt),.rd(rd),.Dec_shift(Dec_shift),.EXTRes(EXTRes),
      .Dec_ALUOp(Dec_ALUOp),.NPCOp(NPCOp),
      .Dec_DMWr(Dec_DMWr),.Dec_RFWr(Dec_RFWr),.PCWr(PCWr),.IF_IDWr(IF_IDWr),
      .Dec_DMRe(Dec_DMRe),.Dec_WBSel(Dec_WBSel),.Dec_RSel(Dec_RSel),.IF_flush(IF_flush),
      .busA(busA),.busB(busB),.SWData(SWData),.Dec_BSel(Dec_BSel),.sign(sign),.shift_var(shift_var),
      .Dec_MEMOp(Dec_MEMOp),.Dec_In_A(Dec_In_A),.Dec_start(Dec_start),.Dec_HLOp(Dec_HLOp),
      .overflow(overflow),.Dec_Exc_Kind(Dec_Exc_Kind),.Dec_CP0Re(Dec_CP0Re),.Dec_CP0Wr(Dec_CP0Wr),
      .Dec_nop(Dec_nop)
   );
// ID end

// ID_EX
   ID_EX U_ID_EX(
      .clk(clk), .rst(rst),
      .Dec_RFWr(Dec_RFWr), .Dec_DMWr(Dec_DMWr), .Dec_DMRe(Dec_DMRe),.Dec_WBSel(Dec_WBSel), 
      .Dec_RSel(Dec_RSel),.Dec_shift(Dec_shift), .busA(busA),.busB(busB),
      .SWData(SWData),.rs(rs),.rt(rt),.rd(rd),.Dec_ALUOp(Dec_ALUOp),.IF_ID_PC(IF_ID_PC),
      .EXTRes(EXTRes),.Dec_BSel(Dec_BSel),.sign(sign),.shift_var(shift_var),.Dec_MEMOp(Dec_MEMOp),
      .Dec_start(Dec_start),.Dec_HLOp(Dec_HLOp),.Dec_Exc_Kind(Dec_Exc_Kind),
      .Dec_CP0Re(Dec_CP0Re),.Dec_CP0Wr(Dec_CP0Wr),.Dec_nop(Dec_nop),.overflow(overflow),
      .IF_ID_ft_exception(IF_ID_ft_exception),.stall(stall),.all_flush(all_flush),

      .ID_EX_RFWr(ID_EX_RFWr),.ID_EX_DMWr(ID_EX_DMWr),.ID_EX_DMRe(ID_EX_DMRe),
      .ID_EX_WBSel(ID_EX_WBSel),.ID_EX_RSel(ID_EX_RSel),.ID_EX_shift(ID_EX_shift),
      .ID_EX_busA(ID_EX_busA),.ID_EX_busB(ID_EX_busB),.ID_EX_SWData(ID_EX_SWData),
      .ID_EX_rs(ID_EX_rs),.ID_EX_rt(ID_EX_rt),.ID_EX_rd(ID_EX_rd),
      .ID_EX_ALUOp(ID_EX_ALUOp),.ID_EX_PC(ID_EX_PC),
      .ID_EX_EXTRes(ID_EX_EXTRes),.ID_EX_BSel(ID_EX_BSel),.ID_EX_sign(ID_EX_sign),
      .ID_EX_shift_var(ID_EX_shift_var),.ID_EX_MEMOp(ID_EX_MEMOp),.ID_EX_start(ID_EX_start),
      .ID_EX_HLOp(ID_EX_HLOp),.ID_EX_Exc_kind(ID_EX_Exc_kind),.ID_EX_CP0Re(ID_EX_CP0Re),
      .ID_EX_CP0Wr(ID_EX_CP0Wr),.ID_EX_nop(ID_EX_nop),.ID_EX_overflow(ID_EX_overflow),
      .ID_EX_ft_exception(ID_EX_ft_exception)
   );
// ID_EX end

// EX
   ex U_ex (
      .ID_EX_busA(ID_EX_busA),.ID_EX_busB(ID_EX_busB),.ID_EX_RSel(ID_EX_RSel),
      .ID_EX_rt(ID_EX_rt),.ID_EX_rd(ID_EX_rd),.ID_EX_ALUOp(ID_EX_ALUOp),
      .ALU_ASel(ALU_ASel),.ALU_BSel(ALU_BSel),.ID_EX_sign(ID_EX_sign),
      .ID_EX_shift_var(ID_EX_shift_var),.ID_EX_shift(ID_EX_shift),.clk(clk),
      .rst(rst),.ID_EX_start(ID_EX_start),.ID_EX_HLOp(ID_EX_HLOp),.all_flush(all_flush),
      .ID_EX_overflow(ID_EX_overflow),.stall(stall),

      .EX_MEM_ALUOut(EX_MEM_ALUOut),.WBData(WBData),.ID_EX_SWData(ID_EX_SWData),
      .ID_EX_EXTRes(ID_EX_EXTRes),.ID_EX_BSel(ID_EX_BSel),
      .EX_Rd(EX_Rd),.EX_SWData(EX_SWData),.ALUOut(ALUOut),.IntegerOverflow(IntegerOverflow),
      .High(EX_High),.Low(EX_Low),.busy(busy),.Hi_HLWr(Hi_HLWr),.Lo_HLWr(Lo_HLWr)

   );
// EX end

// EX_MEM
   EX_MEM U_EX_MEM (
      .clk(clk),.rst(rst),
      .ID_EX_RFWr(ID_EX_RFWr),.ID_EX_WBSel(ID_EX_WBSel),.ALUOut(ALUOut),
      .EX_SWData(EX_SWData),.EX_Rd(EX_Rd),.ID_EX_DMWr(ID_EX_DMWr),
      .ID_EX_PC(ID_EX_PC),.ID_EX_DMRe(ID_EX_DMRe),.ID_EX_MEMOp(ID_EX_MEMOp),
      .EX_High(EX_High),.EX_Low(EX_Low),.ID_EX_HLOp(ID_EX_HLOp),
      .Hi_HLWr(Hi_HLWr),.Lo_HLWr(Lo_HLWr),.all_flush(all_flush),
      .ID_EX_Exc_kind(ID_EX_Exc_kind),.ID_EX_CP0Re(ID_EX_CP0Re),
      .ID_EX_CP0Wr(ID_EX_CP0Wr),.IntegerOverflow(IntegerOverflow),
      .ID_EX_nop(ID_EX_nop),.ID_EX_rd(ID_EX_rd),.Exc_out(Exc_out),.stall(stall),
      .ID_EX_ft_exception(ID_EX_ft_exception),

      
      .EX_MEM_RFWr(EX_MEM_RFWr),.EX_MEM_WBSel(EX_MEM_WBSel),
      .EX_MEM_ALUOut(EX_MEM_ALUOut),.EX_MEM_SWData(EX_MEM_SWData),
      .EX_MEM_Rd(EX_MEM_Rd),.EX_MEM_DMWr(EX_MEM_DMWr),.EX_MEM_PC(EX_MEM_PC),
      .EX_MEM_DMRe(EX_MEM_DMRe),.EX_MEM_MEMOp(EX_MEM_MEMOp),
      .High(High),.Low(Low),.EX_MEM_Exc_kind(EX_MEM_Exc_kind),
      .EX_MEM_CP0Re(EX_MEM_CP0Re),.EX_MEM_CP0Wr(EX_MEM_CP0Wr),
      .EX_MEM_IntegerOverflow(EX_MEM_IntegerOverflow),.EX_MEM_nop(EX_MEM_nop),
      .EX_MEM_really_Rd(EX_MEM_really_Rd),.EX_MEM_ft_exception(EX_MEM_ft_exception)
   );
// EX_MEM end

// MEM
   mem U_MEM (
      .EX_MEM_ALUOut(EX_MEM_ALUOut),.EX_MEM_MEMOp(EX_MEM_MEMOp),
      .data_sram_rdata(data_sram_rdata),.EX_MEM_RFWr(EX_MEM_RFWr),
      .EX_MEM_DMRe(EX_MEM_DMRe),.MEM_WB_MEMOp(MEM_WB_MEMOp),
      .MEM_WB_ALUOut(WB_address[1:0]),.EX_MEM_SWData(EX_MEM_SWData),.all_flush(all_flush),

      .dm_dout(dm_dout),.AddressException(AddressException),
      .data_sram_wen(data_sram_wen),.rf_wen(rf_wen),.physical_address(physical_address),
      .MEM_SWData(MEM_SWData)
   );
   assign data_sram_en = (EX_MEM_DMRe || EX_MEM_DMWr) ? 1 : 0;
   assign data_sram_wdata = MEM_SWData;
   assign data_sram_addr = physical_address;
// MEM end

// Exception
   exception U_exception (
      .clk(clk),.rst(rst),
      .AddressException(AddressException),.EX_MEM_IntegerOverflow(EX_MEM_IntegerOverflow),
      .EX_MEM_Exc_kind(EX_MEM_Exc_kind),.EX_MEM_CP0Wr(ID_EX_CP0Wr),.EX_MEM_nop(EX_MEM_nop),
      .EX_MEM_PC(EX_MEM_PC),.EX_MEM_DMRe(EX_MEM_DMRe),
      .virtualAdress(EX_MEM_ALUOut),
      .Exc_in_data(EX_SWData),.Rd(ID_EX_rd),.EX_MEM_ft_exception(EX_MEM_ft_exception),

      .Exc_out(Exc_out),.EPCOp(EPCOp),.all_flush(all_flush),.EPC(EPC)
   );
// Exception end
   
// MEM_WB
   MEM_WB U_MEM_WB (
      .clk(clk),.rst(rst),
      .EX_MEM_WBSel(EX_MEM_WBSel),.EX_MEM_RFWr(EX_MEM_RFWr),.EX_MEM_ALUOut(EX_MEM_ALUOut),
      .dm_dout(dm_dout),.EX_MEM_Rd(EX_MEM_Rd),.EX_MEM_PC(EX_MEM_PC),.rf_wen(rf_wen),
      .EX_MEM_MEMOp(EX_MEM_MEMOp),.all_flush(all_flush),
     

      .MEM_WB_WBSel(MEM_WB_WBSel),.MEM_WB_RFWr(MEM_WB_RFWr),.MEM_WB_ALUOut(MEM_WB_ALUOut),
      .MEM_WB_dm_dout(MEM_WB_dm_dout),.MEM_WB_Rd(MEM_WB_Rd),.MEM_WB_PC(MEM_WB_PC),
      .MEM_WB_rf_wen(MEM_WB_rf_wen),.MEM_WB_MEMOp(MEM_WB_MEMOp),.WB_address(WB_address)
   );
// MEM_WB end

// WB
   wb U_WB (
      .MEM_WB_WBSel(MEM_WB_WBSel),.MEM_WB_ALUOut(MEM_WB_ALUOut),
      .MEM_WB_dm_dout(dm_dout),.MEM_WB_PC(MEM_WB_PC),
      .WBData(WBData)
   );

   assign debug_wb_pc = MEM_WB_PC;
   assign debug_wb_rf_wdata = WBData;
   assign debug_wb_rf_wen = MEM_WB_rf_wen;
   assign debug_wb_rf_wnum = MEM_WB_Rd;
// WB end

// data forward and stall by lw hazard
   forward U_ForWard (
      .rs(rs),.rt(rt),.ID_EX_rs(ID_EX_rs),.ID_EX_rt(ID_EX_rt),.EX_MEM_Rd(EX_MEM_Rd),
      .MEM_WB_Rd(MEM_WB_Rd),.EX_MEM_RFWr(EX_MEM_RFWr),.IF_ID_ins(IF_ID_ins),
      .ID_EX_DMRe(ID_EX_DMRe),.EX_MEM_DMRe(EX_MEM_DMRe),.EX_Rd(EX_Rd),
      .ID_EX_RFWr(ID_EX_RFWr),.MEM_WB_RFWr(MEM_WB_RFWr),.busy(busy),.all_flush(all_flush),

      .ALU_ASel(ALU_ASel),.ALU_BSel(ALU_BSel),.Bran_ASel(Bran_ASel),
      .Bran_BSel(Bran_BSel),.stall(stall)
   );


endmodule