module pc(clk,rst,PCWr,NPCOp,IF_ID_PC,IF_ID_ins,Dec_In_A,EPCOp,EPC,

            PC,phy_PC,ft_exception);

    input clk;
    input rst;
    input PCWr;
    input [1:0] NPCOp;
    input [31:0] IF_ID_PC;
    input [31:0] IF_ID_ins;
    input [31:0] Dec_In_A;
    input [1:0] EPCOp;
    input [31:0] EPC;
    
    output reg [31:0] PC;
    output reg ft_exception;

    reg [31:0] NPC;
    output reg [31:0] phy_PC;

    
    

    //virtual_PC
    always @ (posedge clk) begin
        if (rst)
            PC <= 32'hbfc0_0000;
        else if (EPCOp != 2'b11)
            PC <= NPC;
        else if (PCWr)
            PC <= NPC;
    end

    // phy_PC
    always @ (PC) begin
      if (PC >= 32'h8000_0000 && PC <= 32'hbfff_ffff)
         phy_PC = {3'b000,PC[28:0]};
      else
         phy_PC = PC;
    end


  
    //NPC
    always @ (PC or NPCOp or IF_ID_PC or IF_ID_ins or Dec_In_A or EPCOp or EPC) begin
        if (EPCOp == 2'b00)
            NPC = 32'hbfc0_0380;
        else if (EPCOp == 2'b01)
            NPC = EPC;
        else if (NPCOp == 2'b01)
            NPC = IF_ID_PC + 4 + ({{16{IF_ID_ins[15]}},IF_ID_ins[15:0]} << 2 ); 
        else if (NPCOp == 2'b10)
            NPC = {IF_ID_PC[31:28],IF_ID_ins[25:0],2'b00};
        else if (NPCOp == 2'b11)
            NPC = Dec_In_A;
        else
            NPC = PC + 4;
    end

    // ft_exception
    always @ (PC) begin
        if (PC[1:0] != 2'b00)
            ft_exception = 1;
        else
            ft_exception = 0;
    end

    

endmodule