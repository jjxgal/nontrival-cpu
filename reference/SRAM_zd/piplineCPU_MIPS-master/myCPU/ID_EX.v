module ID_EX(clk,rst,
            Dec_RFWr,Dec_DMWr,Dec_DMRe,Dec_WBSel,Dec_RSel,
            Dec_shift,busA,busB,SWData,rs,rt,rd,Dec_ALUOp,IF_ID_PC,
            EXTRes,Dec_BSel,sign,shift_var,Dec_MEMOp,Dec_start,
            Dec_HLOp,Dec_Exc_Kind,Dec_CP0Re,Dec_CP0Wr,Dec_nop,overflow,
            IF_ID_ft_exception,stall,all_flush,

            ID_EX_RFWr,ID_EX_DMWr,ID_EX_DMRe,ID_EX_WBSel,ID_EX_RSel,
            ID_EX_shift,ID_EX_busA,ID_EX_busB,ID_EX_SWData,ID_EX_rs,
            ID_EX_rt,ID_EX_rd,ID_EX_ALUOp,ID_EX_PC,
            ID_EX_EXTRes,ID_EX_BSel,ID_EX_sign,ID_EX_shift_var,
            ID_EX_MEMOp,ID_EX_start,ID_EX_HLOp,ID_EX_Exc_kind,
            ID_EX_CP0Re,ID_EX_CP0Wr,ID_EX_nop,ID_EX_overflow,
            ID_EX_ft_exception
        );

    input clk;
    input rst;
    input Dec_RFWr;
    input Dec_DMWr;
    input Dec_DMRe;
    input [1:0] Dec_WBSel;
    input [1:0] Dec_RSel;

    input [4:0] Dec_shift;
    input [31:0] busA;
    input [31:0] busB;
    input [31:0] SWData;
    input [4:0] rs;
    input [4:0] rt;
    input [4:0] rd;
    input [3:0] Dec_ALUOp;
    input [31:0] IF_ID_PC;
    input [31:0] EXTRes;
    input Dec_BSel;
    input sign;
    input shift_var;
    input [2:0] Dec_MEMOp;

    input Dec_start;
    input [2:0] Dec_HLOp;
    input [2:0] Dec_Exc_Kind;
    input Dec_CP0Re;
    input Dec_CP0Wr;

    input Dec_nop;
    input overflow;
    input IF_ID_ft_exception;
    input stall;
    input all_flush;
    
   

    output reg ID_EX_RFWr;
    output reg ID_EX_DMWr;
    output reg ID_EX_DMRe;
    output reg [1:0] ID_EX_WBSel;
    output reg [1:0] ID_EX_RSel;
    output reg [4:0] ID_EX_shift;
    output reg [31:0] ID_EX_busA;
    output reg [31:0] ID_EX_busB;
    output reg [31:0] ID_EX_SWData;
    output reg [4:0] ID_EX_rs;
    output reg [4:0] ID_EX_rt;
    output reg [4:0] ID_EX_rd;
    output reg [3:0] ID_EX_ALUOp;
    output reg [31:0] ID_EX_PC;
    output reg [31:0] ID_EX_EXTRes;
    output reg ID_EX_BSel;
    output reg ID_EX_sign;
    output reg ID_EX_shift_var;
    output reg [2:0] ID_EX_MEMOp;
    output reg ID_EX_start;
    output reg [2:0] ID_EX_HLOp;

    output reg [2:0] ID_EX_Exc_kind;
    output reg ID_EX_CP0Re;
    output reg ID_EX_CP0Wr;

    output reg ID_EX_nop;
    output reg ID_EX_overflow;
    output reg ID_EX_ft_exception;
    

    
    // ID_EX_RFWr
    always @ (posedge clk)
        if (rst)
            ID_EX_RFWr <= 0;
        else
            ID_EX_RFWr <= Dec_RFWr;

    // ID_EX_DMWr
    always @ (posedge clk)
        if (rst)
            ID_EX_DMWr <= 0;
        else
            ID_EX_DMWr <= Dec_DMWr;

    // ID_EX_DMRe
    always @ (posedge clk)
        if (rst)
            ID_EX_DMRe <= 0;
        else
            ID_EX_DMRe <= Dec_DMRe;
    
    // ID_EX_WBSel
    always @ (posedge clk)
        if (rst)
            ID_EX_WBSel <= 0;
        else
            ID_EX_WBSel <= Dec_WBSel;
    
    // ID_EX_RSel
    always @ (posedge clk)
        if (rst)
            ID_EX_RSel <= 0;
        else
            ID_EX_RSel <= Dec_RSel;

    // ID_EX_shift
    always @ (posedge clk)
        if (rst)
            ID_EX_shift <= 0;
        else
            ID_EX_shift <= Dec_shift;

    // ID_EX_busA
    always @ (posedge clk)
        if (rst)
            ID_EX_busA <= 0;
        else
            ID_EX_busA <= busA;

    // ID_EX_busB
    always @ (posedge clk)
        if (rst)
            ID_EX_busB <= 0;
        else
            ID_EX_busB <= busB;

    // ID_EX_SWData
    always @ (posedge clk)
        if (rst)
            ID_EX_SWData <= 0;
        else
            ID_EX_SWData <= SWData;
    
    // ID_EX_rs
    always @ (posedge clk)
        if (rst)
            ID_EX_rs <= 0;
        else
            ID_EX_rs <= rs;

    // ID_EX_rt
    always @ (posedge clk)
        if (rst)
            ID_EX_rt <= 0;
        else
            ID_EX_rt <= rt;
    
    // ID_EX_rd
    always @ (posedge clk)
        if (rst)
            ID_EX_rd <= 0;
        else
            ID_EX_rd <= rd;
    
    // ID_EX_ALUOp
    always @ (posedge clk) 
        if (rst)
            ID_EX_ALUOp <= 0;
        else
            ID_EX_ALUOp <= Dec_ALUOp;

    // ID_EX_EXTRes
    always @ (posedge clk)
        if (rst)
            ID_EX_EXTRes <= 0;
        else
            ID_EX_EXTRes <= EXTRes;

    // ID_EX_BSel
    always @ (posedge clk)
        if (rst)
            ID_EX_BSel <= 0;
        else
            ID_EX_BSel <= Dec_BSel;

    // ID_EX_PC
    always @ (posedge clk)
        if (rst)
            ID_EX_PC <= 0;
        else
            ID_EX_PC <= IF_ID_PC;

    // ID_EX_sign
    always @ (posedge clk)
        if (rst)
            ID_EX_sign <= 0;
        else
            ID_EX_sign <= sign;

    // ID_EX_shift_var
    always @ (posedge clk)
        if (rst)
            ID_EX_shift_var <= 0;
        else
            ID_EX_shift_var <= shift_var;

    // ID_EX_MEMOp
    always @ (posedge clk)
        if (rst)
            ID_EX_MEMOp <= 0;
        else
            ID_EX_MEMOp <= Dec_MEMOp;

    // ID_EX_start
    always @ (posedge clk)
        if (rst)
            ID_EX_start <= 0;
        else
            ID_EX_start <= Dec_start;
    
    // ID_EX_HLOp
    always @ (posedge clk)
        if (rst)
            ID_EX_HLOp <= 0;
        else
            ID_EX_HLOp <= Dec_HLOp;

    // ID_EX_Exc_kind
    always @ (posedge clk)
        if (rst)
            ID_EX_Exc_kind <= 0;
        else
            ID_EX_Exc_kind <= Dec_Exc_Kind;
    
    // ID_EX_CP0Re
    always @ (posedge clk)
        if (rst)
            ID_EX_CP0Re <= 0;
        else
            ID_EX_CP0Re <= Dec_CP0Re;
    
    // ID_EX_CP0Wr
    always @ (posedge clk)
        if (rst)
            ID_EX_CP0Wr <= 0;
        else
            ID_EX_CP0Wr <= Dec_CP0Wr;

    // ID_EX_nop
    always @ (posedge clk)
        if (rst)
            ID_EX_nop <= 0;
        else
            ID_EX_nop <= Dec_nop;

    // ID_EX_overflow
    always @ (posedge clk)
        if (rst)
            ID_EX_overflow <= 0;
        else
            ID_EX_overflow <= overflow;
    
    // ID_EX_ft_exception
    always @ (posedge clk )
        if (rst || all_flush)
            ID_EX_ft_exception <= 0;
        else if (!stall)
            ID_EX_ft_exception <= IF_ID_ft_exception;

endmodule