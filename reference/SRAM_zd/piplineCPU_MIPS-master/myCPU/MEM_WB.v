module MEM_WB (
    clk,rst,
    EX_MEM_WBSel,EX_MEM_RFWr,EX_MEM_ALUOut,dm_dout,EX_MEM_Rd,
    EX_MEM_PC,rf_wen,EX_MEM_MEMOp,all_flush,
  


    MEM_WB_WBSel,MEM_WB_RFWr,MEM_WB_ALUOut,MEM_WB_dm_dout,MEM_WB_Rd,
    MEM_WB_PC,MEM_WB_rf_wen,MEM_WB_MEMOp,WB_address
);
    input clk;
    input rst;
    input [1:0] EX_MEM_WBSel;
    input EX_MEM_RFWr;
    input [31:0] EX_MEM_ALUOut;
    input [31:0] dm_dout;
    input [4:0] EX_MEM_Rd;
    input [31:0] EX_MEM_PC;
    input [3:0] rf_wen;
    input [2:0] EX_MEM_MEMOp;
    input all_flush;
    

    output reg [1:0] MEM_WB_WBSel;
    output reg MEM_WB_RFWr;
    output reg [31:0] MEM_WB_ALUOut;
    output reg [31:0] MEM_WB_dm_dout;
    output reg [4:0] MEM_WB_Rd;
    output reg [31:0] MEM_WB_PC;
    output reg [3:0] MEM_WB_rf_wen;
    output reg [2:0] MEM_WB_MEMOp;
    output reg [31:0] WB_address; 

    // MEM_WB_WBSel
    always @ (posedge clk)
        if (rst)
            MEM_WB_WBSel <= 0;
        else
            MEM_WB_WBSel <= EX_MEM_WBSel;
    
    // MEM_WB_RFWr
    always @ (posedge clk)
        if (rst || all_flush)
            MEM_WB_RFWr <= 0;
        else
            MEM_WB_RFWr <= EX_MEM_RFWr;
    
    // MEM_WB_ALUOut
    always @ (posedge clk) 
        if (rst)
            MEM_WB_ALUOut <= 0;
        else
            MEM_WB_ALUOut <= EX_MEM_ALUOut;

    // WB_address
    always @ (posedge clk)
        if (rst)
            WB_address <= 0;
        else
            WB_address <= EX_MEM_ALUOut;


    // MEM_WB_dm_dout
    always @ (posedge clk)
        if (rst)
            MEM_WB_dm_dout <= 0;
        else
            MEM_WB_dm_dout <= dm_dout;
        
    // MEM_WB_Rd
    always @ (posedge clk)
        if (rst)
            MEM_WB_Rd <= 0;
        else
            MEM_WB_Rd <= EX_MEM_Rd;
    
    // MEM_WB_PC
    always @ (posedge clk)
        if (rst)
            MEM_WB_PC <= 0;
        else
            MEM_WB_PC <= EX_MEM_PC;

    // MEM_WB_rf_men
    always @ (posedge clk)
        if (rst || all_flush)
            MEM_WB_rf_wen <= 0;
        else
            MEM_WB_rf_wen <= rf_wen;

    // MEM_WB_MEMOp
    always @ (posedge clk)
        if (rst)    
            MEM_WB_MEMOp <= 3'b1;
        else
            MEM_WB_MEMOp <= EX_MEM_MEMOp;
endmodule 