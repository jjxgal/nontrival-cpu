module forward
    (
      rs,rt,ID_EX_rs,ID_EX_rt,EX_MEM_Rd,MEM_WB_Rd,EX_MEM_RFWr,
      IF_ID_ins,ID_EX_DMRe,EX_MEM_DMRe,EX_Rd,ID_EX_RFWr,MEM_WB_RFWr,
      busy,all_flush,

      ALU_ASel,ALU_BSel,Bran_ASel,Bran_BSel,stall
    );
    
    input [4:0] rs;
    input [4:0] rt;

    input [4:0] ID_EX_rs;
    input [4:0] ID_EX_rt;

    input [4:0] EX_MEM_Rd;
    input [4:0] MEM_WB_Rd;
    input EX_MEM_RFWr;
    input [31:0] IF_ID_ins;
    input ID_EX_DMRe;
    input EX_MEM_DMRe;
    input [4:0] EX_Rd;
    input ID_EX_RFWr;
    input MEM_WB_RFWr;
    input busy;
    input all_flush;


    output reg [1:0] ALU_ASel;
    output reg [1:0] ALU_BSel;
    output reg [1:0] Bran_ASel;
    output reg [1:0] Bran_BSel;
    output reg stall;
    
    reg RsAndRt;
    reg branch_ins;
    wire [5:0] op = IF_ID_ins[31:26];

// branch and jump instruction
    wire beq;
    wire bne;
    wire bgez;
    wire bgtz;
    wire blez;
    wire bltz;
    wire bgezal;
    wire bltzal;

    wire jr;
    wire jalr;
// branch and jump instruction end

// branch and jump instruction

    assign beq = (op == 6'b000100) ? 1 : 0;
    assign bne = (op == 6'b000101) ? 1 : 0;
    assign bgez = (op == 6'b000001 && IF_ID_ins[20:16] == 5'b00001) ? 1 : 0;
    assign bgtz = (op == 6'b000111 && IF_ID_ins[20:16] == 5'b00000) ? 1 : 0;
    assign blez = (op == 6'b000110 && IF_ID_ins[20:16] == 5'b00000) ? 1 : 0;
    assign bltz = (op == 6'b000001 && IF_ID_ins[20:16] == 5'b00000) ? 1 : 0;
    assign bgezal = (op == 6'b000001 && IF_ID_ins[20:16] == 5'b10001) ? 1 : 0;
    assign bltzal = (op == 6'b000001 && IF_ID_ins[20:16] == 5'b10000) ? 1 : 0;

    assign jr = (op == 6'b000000 && IF_ID_ins[5:0] == 6'b001000) ? 1 : 0;
    assign jalr = (op == 6'b000000 && IF_ID_ins[5:0] == 6'b001001) ? 1 : 0;
// branch and jump instruction end

    // ALU_ASel
    always @ (EX_MEM_Rd or MEM_WB_Rd or MEM_WB_RFWr or EX_MEM_RFWr or ID_EX_rs or rs) begin
        if (EX_MEM_RFWr && EX_MEM_Rd != 5'b0 && EX_MEM_Rd == ID_EX_rs) 
          ALU_ASel = 2'b01;
        else if (MEM_WB_RFWr && MEM_WB_Rd != 5'b0 && MEM_WB_Rd == ID_EX_rs) 
          ALU_ASel = 2'b10;
        else
          ALU_ASel = 2'b00;
    end

    // Bran_ASel
    always @ (EX_MEM_Rd or MEM_WB_Rd or MEM_WB_RFWr or EX_MEM_RFWr or rs) begin
        if (EX_MEM_RFWr && EX_MEM_Rd != 5'b0 && EX_MEM_Rd == rs) 
          Bran_ASel = 2'b01;
        else if (MEM_WB_RFWr && MEM_WB_Rd != 5'b0 && MEM_WB_Rd == rs) 
          Bran_ASel = 2'b10;
        else
          Bran_ASel = 2'b00;
    end

    // BSel
    always @ (EX_MEM_Rd or MEM_WB_Rd or MEM_WB_RFWr or EX_MEM_RFWr or ID_EX_rt) begin
        if (EX_MEM_RFWr && EX_MEM_Rd != 5'b0 && EX_MEM_Rd == ID_EX_rt )
          ALU_BSel = 2'b01;
        else if (MEM_WB_RFWr && MEM_WB_Rd != 5'b0 && MEM_WB_Rd == ID_EX_rt)
          ALU_BSel = 2'b10;
        else
          ALU_BSel = 2'b00;
    end

    // Bran_BSel
    always @ (EX_MEM_Rd or MEM_WB_Rd or MEM_WB_RFWr or EX_MEM_RFWr or rt ) begin
        if (EX_MEM_RFWr && EX_MEM_Rd != 5'b0 && EX_MEM_Rd == rt)
          Bran_BSel = 2'b01;
        else if (MEM_WB_RFWr && MEM_WB_Rd != 5'b0 && MEM_WB_Rd == rt)
          Bran_BSel = 2'b10;
        else
          Bran_BSel = 2'b00;
    end

    
    // op = 000100 beq
    // op = 000101 bne
    // op = 101000 sb
    // op = 101001 sh
    // op = 101011 sw
    // Here , i use op = 6'000000 to decide if a instruction would read both of rs and rd.
    // Althongh some instructions' ,sunch as SRL, op filed is  6'000000 and not read both of rs and rd.
    // This doesn't matter for the filed of they don't read is always 0,which result they want't read 0 rf,which
    // is not expected to be read,just for write.
    // RF_read_port_A
    always @ (*) begin
        if (op == 6'b000000 || op == 6'b000100 || op == 6'b000101
            || op == 6'b101000 || op == 6'b101001 || op == 6'b101011) //three sw inst
            RsAndRt = 1;
        else 
            RsAndRt = 0;
    end

    // branch
    always @ (*) begin
        if (beq || bne || bgez || bgtz || blez || bltz || bgezal || bltzal || jr || jalr)
            branch_ins = 1;
        else
            branch_ins = 0;
    end


    // stall
    always @ (*) begin
        if (all_flush)
          stall = 0;
        else if (busy)
            stall = 1;
        else if (ID_EX_DMRe && (ID_EX_rt == rs || (RsAndRt && ID_EX_rt == rt)) )
            stall = 1;
        else if (EX_MEM_DMRe && branch_ins && (EX_MEM_Rd == rs || (RsAndRt && EX_MEM_Rd == rt)) )
            stall = 1;
        else if (ID_EX_RFWr && branch_ins &&(EX_Rd == rs || (RsAndRt && EX_Rd == rt)) )
            stall = 1;
        else
            stall = 0;
    end

     
endmodule // forward