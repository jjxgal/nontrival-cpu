module IF_ID(
            clk,rst,
            IF_IDWr,im_dout,IF_flush,PC,all_flush,ft_exception,
            stall,


            IF_ID_PC,IF_ID_ins,IF_ID_ft_exception);

    input clk;
    input rst;
    input IF_IDWr;
    input [31:0] im_dout;
    input IF_flush;
    input [31:0] PC;
    input all_flush;
    input ft_exception;
    input stall;
    
    output reg [31:0] IF_ID_PC;
    output reg [31:0] IF_ID_ins;
    output reg IF_ID_ft_exception;

   

    //IF_ID_PC
    always @ (posedge clk)
        if (rst)
            IF_ID_PC <= 32'b0;
        else if (IF_flush)
            IF_ID_PC <= 32'b0;
        else if (IF_IDWr)
            IF_ID_PC <= PC;
    
    // reg con_stall;
    // // continue stall
    // always @ (posedge clk) begin
    //     if (!IF_IDWr)
    //         con_stall = 1;
    //     else
    //         con_stall = 0;
    // end

    // // tmp_IF_ID_ins
    // reg [31:0] tmp_IF_ID_ins;
    // always @ (negedge clk or posedge rst)
    //     if (rst)    
    //         tmp_IF_ID_ins <= 0;
    //     else if (!IF_IDWr && !con_stall)
    //         tmp_IF_ID_ins <= IF_ID_ins;
    
   

    // // always @ (IFSel or im_dout or tmp_IF_ID_ins)
    // //     if (IFSel)
    // //         IF_ID_ins = im_dout;
    // //     else
    // //         IF_ID_ins = tmp_IF_ID_ins;
    // reg IFSel;

    // always @ (posedge clk)
    //     if (IF_IDWr)
    //         IFSel <= 1;
    //     else
    //         IFSel <= 0;
    reg ins_flush;

    always @ (posedge clk)
        if (all_flush)
            ins_flush <= 1;
        else
            ins_flush <= 0;

    //IF_ID_ins
    always @ (rst or im_dout or ins_flush)
        if (rst)
            IF_ID_ins = 32'b0;
        else if (ins_flush)
            IF_ID_ins = 32'b0;
        else
            IF_ID_ins = im_dout;

    // ft_exception
    always @ (posedge clk)
        if (rst || all_flush)
            IF_ID_ft_exception <= 0;
        else if (!stall)
            IF_ID_ft_exception <= ft_exception;
endmodule