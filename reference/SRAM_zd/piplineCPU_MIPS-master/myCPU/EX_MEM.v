module EX_MEM (
            clk,rst,
            ID_EX_RFWr,ID_EX_WBSel,ALUOut,EX_SWData,
            EX_Rd,ID_EX_DMWr,ID_EX_PC,ID_EX_DMRe,ID_EX_MEMOp,
            EX_High,EX_Low,ID_EX_HLOp,Hi_HLWr,Lo_HLWr,all_flush,
            ID_EX_CP0Wr,ID_EX_CP0Re,ID_EX_Exc_kind,IntegerOverflow,
            ID_EX_nop,ID_EX_rd,Exc_out,stall,ID_EX_ft_exception,
            

            EX_MEM_RFWr,EX_MEM_WBSel,EX_MEM_ALUOut,EX_MEM_SWData,
            EX_MEM_Rd,EX_MEM_DMWr,EX_MEM_PC,EX_MEM_DMRe,EX_MEM_MEMOp,
            High,Low,EX_MEM_CP0Re,EX_MEM_CP0Wr,EX_MEM_Exc_kind,
            EX_MEM_IntegerOverflow,EX_MEM_nop,EX_MEM_really_Rd,
            EX_MEM_ft_exception
        );

    input clk;
    input rst;

    input ID_EX_RFWr;
    input [1:0] ID_EX_WBSel;
    input [31:0] ALUOut;
    input [31:0] EX_SWData;
    input [4:0] EX_Rd;
    input ID_EX_DMWr;
    input [31:0] ID_EX_PC;
    input ID_EX_DMRe;
    input [2:0] ID_EX_MEMOp;
    input [31:0] EX_High;
    input [31:0] EX_Low;
    input [2:0] ID_EX_HLOp;
    input Hi_HLWr;
    input Lo_HLWr;
    input all_flush;
    input [2:0] ID_EX_Exc_kind;
    input ID_EX_CP0Re;
    input ID_EX_CP0Wr;
    input IntegerOverflow;
    input ID_EX_nop;
    input [4:0] ID_EX_rd;
    input [31:0] Exc_out;
    input stall;
    input ID_EX_ft_exception;
    

    output reg EX_MEM_RFWr;
    output reg [1:0] EX_MEM_WBSel;
    output reg [31:0] EX_MEM_ALUOut;
    output reg [31:0] EX_MEM_SWData;
    output reg [4:0] EX_MEM_Rd;
    output reg EX_MEM_DMWr;
    output reg [31:0] EX_MEM_PC;
    output reg EX_MEM_DMRe;
    output reg [2:0] EX_MEM_MEMOp;
    
    output reg [31:0] High;
    output reg [31:0] Low;
    output reg [2:0] EX_MEM_Exc_kind;
    output reg EX_MEM_CP0Re;
    output reg EX_MEM_CP0Wr;
    output reg EX_MEM_IntegerOverflow;
    output reg EX_MEM_nop;
    output reg [4:0] EX_MEM_really_Rd;
    output reg EX_MEM_ft_exception;

    // EX_MEM_RFWr
    always @ (posedge clk)
        if (rst || all_flush)
            EX_MEM_RFWr <= 0;
        else
            EX_MEM_RFWr <= ID_EX_RFWr;

    // EX_MEM_WBSel
    always @ (posedge clk)
        if (rst)
            EX_MEM_WBSel <= 0;
        else
            EX_MEM_WBSel <= ID_EX_WBSel;

    // EX_MEM_ALUOut
    always @ (posedge clk)
        if (rst)
            EX_MEM_ALUOut <= 0;
        else if (ID_EX_HLOp == 3'b000)
            EX_MEM_ALUOut <= High;
        else if (ID_EX_HLOp == 3'b001)
            EX_MEM_ALUOut <= Low;
        else if (ID_EX_CP0Re)
            EX_MEM_ALUOut <= Exc_out;
        else
            EX_MEM_ALUOut <= ALUOut;

    // EX_MEM_SWData
    always @ (posedge clk)
        if (rst)
            EX_MEM_SWData <= 0;
        else
            EX_MEM_SWData <= EX_SWData;

    // EX_MEM_Rd
    always @ (posedge clk)
        if (rst)
            EX_MEM_Rd <= 0;
        else
            EX_MEM_Rd <= EX_Rd;

    // EX_MEM_DMWr
    always @ (posedge clk)
        if (rst || all_flush)
            EX_MEM_DMWr <= 0;
        else
            EX_MEM_DMWr <= ID_EX_DMWr;
    
    // EX_MEM_PC
    always @ (posedge clk)
        if (rst)
            EX_MEM_PC <= 0;
        else
            EX_MEM_PC <= ID_EX_PC;

    // EX_MEM_DMRe
    always @ (posedge clk)
        if (rst)
            EX_MEM_DMRe <= 0;
        else
            EX_MEM_DMRe <= ID_EX_DMRe;

    // EX_MEM_MEMOp
    always @ (posedge clk)
        if (rst)
            EX_MEM_MEMOp <= 0;
        else
            EX_MEM_MEMOp <= ID_EX_MEMOp;

    // High
    always @ (posedge clk)
        if (rst)
            High <= 0;
        else if (Hi_HLWr)
            High <= EX_High;
    
    
    // Low
    always @ (posedge clk)
        if (rst)
            Low <= 0;
        else if (Lo_HLWr)
            Low <= EX_Low;

    // EX_MEM_Exc_kind
    always @ (posedge clk)
        if (rst)
            EX_MEM_Exc_kind <= 0;
        else if (!stall)
            EX_MEM_Exc_kind <= ID_EX_Exc_kind;

    // EX_MEM_CP0Re
    always @ (posedge clk)
        if (rst)
            EX_MEM_CP0Re <= 0;
        else
            EX_MEM_CP0Re <= ID_EX_CP0Re;
    
    // EX_MEM_CP0Wr
    always @ (posedge clk)
        if (rst)
            EX_MEM_CP0Wr <= 0;
        else
            EX_MEM_CP0Wr <= ID_EX_CP0Wr;

    // EX_MEM_IntegerOverflow
    always @ (posedge clk)
        if (rst)
            EX_MEM_IntegerOverflow = 0;
        else
            EX_MEM_IntegerOverflow <= IntegerOverflow;

    // EX_MEM_nop
    always @ (posedge clk)
        if (rst)
            EX_MEM_nop <= 0;
        else
            EX_MEM_nop <= ID_EX_nop;
    
    // EX_MEM_really_Rd
    always @ (posedge clk)
        if (rst)
            EX_MEM_really_Rd <= 0;
        else
            EX_MEM_really_Rd <= ID_EX_rd;
    
    // EX_MEM_ft_exception
    always @ (posedge clk)
        if (rst || all_flush)
            EX_MEM_ft_exception <= 0;
        else
            EX_MEM_ft_exception <= ID_EX_ft_exception;
endmodule