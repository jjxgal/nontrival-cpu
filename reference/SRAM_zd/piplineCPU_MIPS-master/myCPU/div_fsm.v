module div_fsm(
                dividend,divisor,start,clock,rst,quotient,

                remainder,busy,count
            );

    input [31:0] dividend;
    input [31:0] divisor;
    input start;
    input clock;
    input rst;

    output [31:0] quotient;
    output [31:0] remainder;
    output busy;
    output [5:0] count;


    // Initialising required registers
    reg [63:0] reg_remainder;
    reg [31:0] reg_divisor;
    reg [5:0] count;
    reg busy;

    



    // Initialising required wires
    wire [32:0] diff = reg_remainder[63:31] - {1'b0,reg_divisor};
    wire Q_n = ~diff[32];
    wire [32:0] Dividend_n1 = (diff[32]) ? reg_remainder[63:31] : diff;
    wire [63:0] Dividend_n2 = {Dividend_n1, reg_remainder[30:0]};
    wire [63:0] Dividend_n3  = Dividend_n2 << 1'b1;
    wire [63:0] Dividend_n = {Dividend_n3[63:1],Q_n};

    //assigning outputs
    assign quotient = Dividend_n[31:0];
    assign remainder = Dividend_n[63:32];

    //Main sequential logic
    //reg_remainder
    always @ (posedge clock) begin
        if (rst)
            reg_remainder = 64'b0;
        else if(start)
            reg_remainder = {{32{1'b0}},dividend};
        else if(busy && count != 6'b100000)
            reg_remainder = Dividend_n;
    end
        
    
    //reg_divisor
    always @ (posedge clock) begin
        if (rst)
            reg_divisor = 32'b0;
        else if(start)
            reg_divisor = divisor;
    end
        

    //busy
    always @ (posedge clock) begin
        if (rst)
            busy <= 1'b0;
        else if (start)
            busy <= 1'b1;
        else if (count >= 6'b011111)
            busy <= 1'b0;
    end

    //count
    always @ (posedge clock) begin
        if (rst || count > 6'b011111)
            count = 6'b0;
        else if(count < 6'b100000 && (start || busy))
            count = count + 6'b000001;
    end

   
endmodule