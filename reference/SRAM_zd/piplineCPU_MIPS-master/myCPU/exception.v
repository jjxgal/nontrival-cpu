module exception(
        clk,rst,AddressException,EX_MEM_IntegerOverflow,EX_MEM_Exc_kind,
        EX_MEM_CP0Wr,EX_MEM_nop,EX_MEM_PC,EX_MEM_DMRe,virtualAdress,Exc_in_data,
        Rd,EX_MEM_ft_exception,

        Exc_out,EPCOp,all_flush,EPC
    );

    input clk;
    input rst;
    input AddressException;
    input EX_MEM_IntegerOverflow;
    input [2:0] EX_MEM_Exc_kind;
    input EX_MEM_CP0Wr;
    
    input EX_MEM_nop;
    input [31:0] EX_MEM_PC;
    input EX_MEM_DMRe;
    input [31:0] virtualAdress;
    input [31:0] Exc_in_data;
    input [4:0] Rd;

    input EX_MEM_ft_exception;
    
    
    

    output reg [31:0] Exc_out;
    
    output reg [1:0] EPCOp;
    output reg all_flush;
    output [31:0] EPC;
    
    
    wire EX_MEM_syscall = (EX_MEM_Exc_kind == 3'b001) ? 1 : 0;
    wire EX_MEM_break = (EX_MEM_Exc_kind == 3'b010) ? 1 : 0;
    wire EX_MEM_eret = (EX_MEM_Exc_kind == 3'b011) ? 1 : 0;
    wire reserved_ins = (EX_MEM_Exc_kind == 3'b100) ? 1 : 0;

    reg [31:0] CP0_R8_S0; // BadVAddr
    reg [31:0] CP0_R9_S0; // count
    reg [31:0] CP0_R12_S0; // status
    reg [31:0] CP0_R13_S0; // cause
    reg [31:0] CP0_R14_S0; // EPC

    wire Status_EXL = CP0_R12_S0[1];
    parameter BD = 5'd31;
    parameter EXL = 5'd1;

    reg SW1;
    reg SW2;
    reg HW0;
    reg HW1;
    reg HW2;
    reg HW3;
    reg HW4;
    reg HW5;

    
    wire exception = (EX_MEM_syscall || EX_MEM_break 
                    || EX_MEM_IntegerOverflow || AddressException 
                    || reserved_ins) ? 1 : 0;

    wire interrupts = (SW1 || SW2 || HW0 || HW1 || HW2 || HW3 || HW4 || HW5) ? 1 : 0;

    assign EPC = CP0_R14_S0; 

    // Exc_out
    always @ (Rd or CP0_R12_S0 or CP0_R13_S0 or CP0_R14_S0 or CP0_R8_S0 or CP0_R9_S0) begin
        if (Rd == 5'd8)
            Exc_out <= CP0_R8_S0;
        else if (Rd == 5'd9)
            Exc_out <= CP0_R9_S0;
        else if (Rd == 5'd12)
            Exc_out <= CP0_R12_S0;
        else if (Rd == 5'd13)
            Exc_out <= CP0_R13_S0;
        else if (Rd == 5'd14)
            Exc_out <= CP0_R14_S0;
        else 
            Exc_out <= 0;
    end

    // badVAddr R8
    always @ (posedge clk) begin
        if (rst)
            CP0_R8_S0 <= 0;
        else if (EX_MEM_CP0Wr && Rd == 5'd8)
            CP0_R8_S0 <= Exc_in_data;
        else if (AddressException)
            CP0_R8_S0 <= virtualAdress;
        else if (EX_MEM_ft_exception)
            CP0_R8_S0 <= EX_MEM_PC;
    end


    reg [1:0] half_clk;
    always @ (posedge clk) begin
        if (rst)
            half_clk <= 0;
        else
            half_clk <= half_clk + 1;
    end

    // Count R9
    always @ (posedge clk) begin
        if (rst)
            CP0_R9_S0 <= 0;
        else if (EX_MEM_CP0Wr && Rd == 5'd9)
            CP0_R9_S0 <= Exc_in_data;
        else if (!half_clk[0])
            CP0_R9_S0 <= CP0_R9_S0 + 1;
    end

    

    

    // status    EXL R12
    always @ (posedge clk) begin
        if (rst)
            CP0_R12_S0 <= 32'h0040_0000; // bev always equal to 1
        else if (exception || EX_MEM_ft_exception || interrupts)
            CP0_R12_S0[EXL] <= 1;
        else if (EX_MEM_eret)
            CP0_R12_S0[EXL] <= 0;
        else if (EX_MEM_CP0Wr && Rd == 5'd12)
            CP0_R12_S0 <= Exc_in_data;
    end

    // EPCOp
    always @ (*) begin
        if (EX_MEM_syscall || EX_MEM_IntegerOverflow || EX_MEM_break 
            || AddressException || EX_MEM_ft_exception || reserved_ins || interrupts)
            EPCOp = 2'b00;
        else if (EX_MEM_eret)
            EPCOp = 2'b01;
        else
            EPCOp = 2'b11;
    end


    

    // Casue  EXcCode 
    always @ (posedge clk) begin
        if (rst)
            CP0_R13_S0[30:0] <= 0;
        else if ((EX_MEM_DMRe && AddressException) || EX_MEM_ft_exception)
            CP0_R13_S0[6:2] <= 5'h04;
        else if (!EX_MEM_DMRe && AddressException)
            CP0_R13_S0[6:2] <= 5'h05;
        else if (EX_MEM_syscall)
            CP0_R13_S0[6:2] <= 5'h08;
        else if (EX_MEM_break)
            CP0_R13_S0[6:2] <= 5'h09;
        else if (EX_MEM_IntegerOverflow)
            CP0_R13_S0[6:2] <= 5'h0c;
        else if (reserved_ins)
            CP0_R13_S0[6:2] <= 5'h0a;
        else if (interrupts)
            CP0_R13_S0[6:2] <= 5'h00;
        else if (EX_MEM_CP0Wr && Rd == 5'd13)
            CP0_R13_S0[30:0] <= Exc_in_data[30:0];

    end
    // cause BD
    always @ (posedge clk) begin
        if (rst)
            CP0_R13_S0[BD] <= 0;
        else if ((AddressException || EX_MEM_IntegerOverflow 
                || EX_MEM_break || EX_MEM_syscall || interrupts || EX_MEM_ft_exception
                || reserved_ins) && !Status_EXL && EX_MEM_nop)
            CP0_R13_S0[BD] <= 1;
        else if (EX_MEM_CP0Wr && Rd == 5'd13)
            CP0_R13_S0[BD] <= Exc_in_data[BD];
    end
 
    // EPC
    always @ (posedge clk) begin
        if (rst)
            CP0_R14_S0 <= 0;
        else if ( exception && !Status_EXL && !EX_MEM_nop)
            CP0_R14_S0 <= EX_MEM_PC;
        else if ( exception && !Status_EXL && EX_MEM_nop)
            CP0_R14_S0 <= EX_MEM_PC - 4;
        else if (EX_MEM_CP0Wr && Rd == 5'd14)
            CP0_R14_S0 <= Exc_in_data;
        else if (EX_MEM_ft_exception)
            CP0_R14_S0 <= EX_MEM_PC;
        else if (interrupts)
            CP0_R14_S0 <= EX_MEM_PC + 4;
    end


    // all_flush
    always @ (*) begin
        if (EX_MEM_syscall || EX_MEM_break || EX_MEM_eret 
            || AddressException || EX_MEM_IntegerOverflow 
            || EX_MEM_ft_exception || reserved_ins || interrupts)
            all_flush = 1;
        else
            all_flush = 0;
    end
    

    // interrupts
    always @ (*) begin
        if (!CP0_R12_S0[EXL] && CP0_R12_S0[0]) begin
            // if (CP0_R13_S0[15] && CP0_R12_S0[15]) begin
            //     HW0 = 0;
            //     HW1 = 0;
            //     HW2 = 0;
            //     HW3 = 0;
            //     HW4 = 0;
            //     HW5 = 1;
            //     SW1 = 0;
            //     SW2 = 0;
            // end
            // else if (CP0_R13_S0[14] && CP0_R12_S0[14]) begin
            //     HW0 = 0;
            //     HW1 = 0;
            //     HW2 = 0;
            //     HW3 = 0;
            //     HW4 = 1;
            //     HW5 = 0;
            //     SW1 = 0;
            //     SW2 = 0;
            // end
            // else if (CP0_R13_S0[13] && CP0_R12_S0[13]) begin
            //     HW0 = 0;
            //     HW1 = 0;
            //     HW2 = 0;
            //     HW3 = 1;
            //     HW4 = 0;
            //     HW5 = 0;
            //     SW1 = 0;
            //     SW2 = 0;
            // end
            // else if (CP0_R13_S0[12] && CP0_R12_S0[12]) begin
            //     HW0 = 0;
            //     HW1 = 0;
            //     HW2 = 1;
            //     HW3 = 0;
            //     HW4 = 0;
            //     HW5 = 0;
            //     SW1 = 0;
            //     SW2 = 0;
            // end
            // else if (CP0_R13_S0[11] && CP0_R12_S0[11]) begin
            //     HW0 = 0;
            //     HW1 = 1;
            //     HW2 = 0;
            //     HW3 = 0;
            //     HW4 = 0;
            //     HW5 = 0;
            //     SW1 = 0;
            //     SW2 = 0;
            // end
            // else if (CP0_R13_S0[10] && CP0_R12_S0[10]) begin
            //     HW0 = 1;
            //     HW1 = 0;
            //     HW2 = 0;
            //     HW3 = 0;
            //     HW4 = 0;
            //     HW5 = 0;
            //     SW1 = 0;
            //     SW2 = 0;
            // end
            if (CP0_R13_S0[9] && CP0_R12_S0[9]) begin
                HW0 = 0;
                HW1 = 0;
                HW2 = 0;
                HW3 = 0;
                HW4 = 0;
                HW5 = 0;
                SW1 = 0;
                SW2 = 1;
            end
            else if (CP0_R13_S0[8] && CP0_R12_S0[8]) begin
                HW0 = 0;
                HW1 = 0;
                HW2 = 0;
                HW3 = 0;
                HW4 = 0;
                HW5 = 0;
                SW1 = 1;
                SW2 = 0;
            end
            else begin
                HW0 = 0;
                HW1 = 0;
                HW2 = 0;
                HW3 = 0;
                HW4 = 0;
                HW5 = 0;
                SW1 = 0;
                SW2 = 0;
            end
        end
        else begin
            HW0 = 0;
            HW1 = 0;
            HW2 = 0;
            HW3 = 0;
            HW4 = 0;
            HW5 = 0;
            SW1 = 0;
            SW2 = 0;
        end
    end

endmodule
