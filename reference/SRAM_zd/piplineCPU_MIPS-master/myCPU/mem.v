module mem(
         EX_MEM_ALUOut,MEM_WB_MEMOp,
         data_sram_rdata,EX_MEM_RFWr,
         EX_MEM_DMRe,EX_MEM_MEMOp,MEM_WB_ALUOut,
         EX_MEM_SWData,all_flush,

         dm_dout,AddressException,data_sram_wen,
         rf_wen,physical_address,MEM_SWData
      );

   input [31:0] EX_MEM_ALUOut;
   input [2:0] MEM_WB_MEMOp;
   input [2:0] EX_MEM_MEMOp;
   input [31:0] data_sram_rdata;
   input EX_MEM_RFWr;
   input EX_MEM_DMRe;
   input [1:0] MEM_WB_ALUOut;
   input [31:0] EX_MEM_SWData;
   input all_flush;

  
   output reg [31:0] dm_dout;
   output reg AddressException;


 
   output reg [3:0] data_sram_wen;
   output reg [3:0] rf_wen;
   output reg [31:0] physical_address;
   output reg [31:0] MEM_SWData;

   wire [31:0] dm_dout_tmp = data_sram_rdata;

   always @ (EX_MEM_ALUOut or EX_MEM_MEMOp or EX_MEM_DMRe or all_flush) begin
      if (all_flush)
         data_sram_wen = 4'b0000;
      else if (EX_MEM_MEMOp == 3'b000 && !EX_MEM_DMRe)
         data_sram_wen = 4'b1111;
      else if (EX_MEM_MEMOp == 3'b001 && EX_MEM_ALUOut[1] && !EX_MEM_DMRe)
         data_sram_wen = 4'b1100;
      else if (EX_MEM_MEMOp == 3'b001 && !EX_MEM_ALUOut[1] && !EX_MEM_DMRe)
         data_sram_wen = 4'b0011;
      else if (EX_MEM_MEMOp == 3'b010 && !EX_MEM_DMRe) begin
         if (EX_MEM_ALUOut[1:0] == 2'b00)
            data_sram_wen = 4'b0001;
         else if (EX_MEM_ALUOut[1:0] == 2'b01)
            data_sram_wen = 4'b0010;
         else if (EX_MEM_ALUOut[1:0] == 2'b10)
            data_sram_wen = 4'b0100;
         else
            data_sram_wen = 4'b1000;
      end
      else
         data_sram_wen = 4'b0000;
   end

   always @ (EX_MEM_ALUOut or EX_MEM_MEMOp or EX_MEM_DMRe or EX_MEM_SWData) begin
      if (EX_MEM_MEMOp == 3'b000 && !EX_MEM_DMRe)
         MEM_SWData = EX_MEM_SWData;
      else if (EX_MEM_MEMOp == 3'b001 && EX_MEM_ALUOut[1] && !EX_MEM_DMRe)
         MEM_SWData = EX_MEM_SWData << 16;
      else if (EX_MEM_MEMOp == 3'b001 && !EX_MEM_ALUOut[1] && !EX_MEM_DMRe)
         MEM_SWData = EX_MEM_SWData;
      else if (EX_MEM_MEMOp == 3'b010 && !EX_MEM_DMRe) begin
         if (EX_MEM_ALUOut[1:0] == 2'b00)
            MEM_SWData = EX_MEM_SWData;
         else if (EX_MEM_ALUOut[1:0] == 2'b01)
            MEM_SWData = EX_MEM_SWData << 8;
         else if (EX_MEM_ALUOut[1:0] == 2'b10)
            MEM_SWData = EX_MEM_SWData << 16;
         else
            MEM_SWData = EX_MEM_SWData << 24;
      end
      else
         MEM_SWData = EX_MEM_SWData;
   end

  
   always @ (EX_MEM_ALUOut or EX_MEM_MEMOp or EX_MEM_RFWr) begin
      // if (EX_MEM_MEMOp == 3'b000 && EX_MEM_RFWr)
      //    rf_wen = 4'b1111;
      // else if ((EX_MEM_MEMOp == 3'b001 || EX_MEM_MEMOp == 3'b100) && EX_MEM_RFWr) begin
      //    if (EX_MEM_ALUOut[1])
      //       rf_wen = 4'b1100;
      //    else
      //       rf_wen = 4'b0011;
      // end
      // else if ((EX_MEM_MEMOp == 3'b010 || EX_MEM_MEMOp == 3'b011) && EX_MEM_RFWr) begin
      //    if (EX_MEM_ALUOut[1:0] == 2'b00)
      //       rf_wen = 4'b0001;
      //    else if (EX_MEM_ALUOut[1:0] == 2'b01)
      //       rf_wen = 4'b0010;
      //    else if (EX_MEM_ALUOut[1:0] == 2'b10)
      //       rf_wen = 4'b0100;
      //    else
      //       rf_wen = 4'b1000;
      // end
      if (EX_MEM_RFWr)
         rf_wen = 4'b1111;
      else
         rf_wen = 4'b0000;
   end
   
   


   // dm_4k U_DM ( 
   //    .addr(EX_MEM_ALUOut[11:0]), .din(EX_MEM_SWData), .DMWr(EX_MEM_DMWr), 
   //    .clk(clk),.EX_MEM_MEMOp(EX_MEM_MEMOp), .rst(rst),
   //    .dout(dm_dout_tmp)
   // );

   // dm_out
   always @ (MEM_WB_ALUOut or MEM_WB_MEMOp or dm_dout_tmp) begin
      if (MEM_WB_MEMOp == 3'b000)
         dm_dout = dm_dout_tmp;
      else if (MEM_WB_MEMOp == 3'b001) begin
         if (MEM_WB_ALUOut[1:0] == 2'b00)
            dm_dout = {{16{dm_dout_tmp[15]}},dm_dout_tmp[15:0]};
         else
            dm_dout = {{16{dm_dout_tmp[31]}},dm_dout_tmp[31:16]};
      end
      else if (MEM_WB_MEMOp == 3'b010) begin
         if (MEM_WB_ALUOut[1:0] == 2'b00)
            dm_dout = {{24{dm_dout_tmp[7]}},dm_dout_tmp[7:0]};
         else if (MEM_WB_ALUOut[1:0] == 2'b01)
            dm_dout = {{24{dm_dout_tmp[15]}},dm_dout_tmp[15:8]};
         else if (MEM_WB_ALUOut[1:0] == 2'b10)
            dm_dout = {{24{dm_dout_tmp[23]}},dm_dout_tmp[23:16]};
         else
            dm_dout = {{24{dm_dout_tmp[31]}},dm_dout_tmp[31:24]};
      end
      else if (MEM_WB_MEMOp == 3'b011) begin
         if (MEM_WB_ALUOut[1:0] == 2'b00)
            dm_dout = {{24{1'b0}},dm_dout_tmp[7:0]};
         else if (MEM_WB_ALUOut[1:0] == 2'b01)
            dm_dout = {{24{1'b0}},dm_dout_tmp[15:8]};
         else if (MEM_WB_ALUOut[1:0] == 2'b10)
            dm_dout = {{24{1'b0}},dm_dout_tmp[23:16]};
         else
            dm_dout = {{24{1'b0}},dm_dout_tmp[31:24]};
      end
      else if (MEM_WB_MEMOp == 3'b100) begin
         if (MEM_WB_ALUOut[1:0] == 2'b00)
            dm_dout = {{16{1'b0}},dm_dout_tmp[15:0]};
         else
            dm_dout = {{16{1'b0}},dm_dout_tmp[31:16]};
      end
      else
         dm_dout = dm_dout_tmp;
   end
   
   // AddressException
   always @ (EX_MEM_ALUOut or EX_MEM_MEMOp) begin
      if (EX_MEM_MEMOp == 3'b000 && EX_MEM_ALUOut[1:0] != 2'b00)
         AddressException = 1;
      else if (EX_MEM_MEMOp == 3'b001 && EX_MEM_ALUOut[0] != 1'b0)
         AddressException = 1;
      else if (EX_MEM_MEMOp == 3'b100 && EX_MEM_ALUOut[0] != 1'b0)
         AddressException = 1;
      else
         AddressException = 0;
   end


   // physical address
   always @ (EX_MEM_ALUOut) begin
      if (EX_MEM_ALUOut >= 32'h8000_0000 && EX_MEM_ALUOut <= 32'hbfff_ffff)
         physical_address = {3'b000,EX_MEM_ALUOut[28:0]};
      else
         physical_address = EX_MEM_ALUOut;
   end

endmodule 