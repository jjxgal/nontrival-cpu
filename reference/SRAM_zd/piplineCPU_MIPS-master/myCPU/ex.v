module ex(
        ID_EX_busA,ID_EX_busB,ID_EX_RSel,ID_EX_rt,
        ID_EX_rd,ID_EX_ALUOp,ALU_ASel,ALU_BSel,
        EX_MEM_ALUOut,WBData,ID_EX_SWData,ID_EX_EXTRes,
        ID_EX_BSel,ID_EX_sign,ID_EX_shift_var,ID_EX_shift,
        ID_EX_start,clk,rst,ID_EX_HLOp,all_flush,ID_EX_overflow,
        stall,

        EX_Rd,EX_SWData,ALUOut,IntegerOverflow,
        High,Low,busy,Hi_HLWr,Lo_HLWr
    );

    input [31:0] ID_EX_busA;
    input [31:0] ID_EX_busB;
    input [1:0] ID_EX_RSel;
    input [4:0] ID_EX_rt;
    input [4:0] ID_EX_rd;
    input [3:0] ID_EX_ALUOp;
    // forwarding
    input [1:0] ALU_ASel;
    input [1:0] ALU_BSel;
    input [31:0] EX_MEM_ALUOut;
    input [31:0] WBData;
    input [31:0] ID_EX_SWData;

    input [31:0] ID_EX_EXTRes;
    input ID_EX_BSel;
    input ID_EX_sign;
    input ID_EX_shift_var;
    input [4:0] ID_EX_shift;
    input ID_EX_start;
    input clk;
    input rst;
    input [2:0] ID_EX_HLOp;
    input all_flush;
    input ID_EX_overflow;
    input stall;

    output reg [4:0] EX_Rd;
    output reg [31:0] EX_SWData;
    output [31:0] ALUOut;
    output reg IntegerOverflow;

    output reg [31:0] High;
    output reg [31:0] Low;
    output reg busy;
    output reg Hi_HLWr;
    output reg Lo_HLWr;


    wire HLWr;
    reg [31:0] In_A;
    reg [31:0] In_B;
    reg [32:0] tmp_ALU_Res;
    
    wire [1:0] RSel;
    wire [3:0] ALUOp;
    
    assign RSel = ID_EX_RSel;
    assign ALUOp = ID_EX_ALUOp;



    reg [31:0] A_md;
    reg [31:0] B_md;
    reg A_change;
    reg B_change;
    reg [3:0] Reserved_ALUOp;
    reg [63:0] reg_product;

    reg [31:0] reg_remainder;
    reg [31:0] reg_quotient;

    wire [63:0] product;
    wire [31:0] remainder;
    wire [31:0] quotient;
    wire mul_busy;
    wire div_busy;
    wire [5:0] div_count;


    reg start;
   
   
    

// assign output
    assign ALUOut = tmp_ALU_Res[31:0];

    // In_A
    always @ (*) begin
        if (ALU_ASel == 2'b00)
            In_A = ID_EX_busA;
        else if (ALU_ASel == 2'b01)
            In_A = EX_MEM_ALUOut;
        else if (ALU_ASel == 2'b10)
            In_A = WBData;
        else
            In_A = 32'b0;
    end

    // In_B
    always @ (*) begin
        if (ID_EX_BSel == 0)
            In_B = ID_EX_EXTRes;
        else if (ALU_BSel == 2'b00)
            In_B = ID_EX_busB;
        else if (ALU_BSel == 2'b01)
            In_B = EX_MEM_ALUOut;
        else if (ALU_BSel == 2'b10)
            In_B = WBData;
        else
            In_B = 32'b0;
    end

    // EX_SWData
    always @ (ID_EX_SWData or ALU_BSel or EX_MEM_ALUOut or WBData) begin
        if (ALU_BSel == 2'b00)
            EX_SWData = ID_EX_SWData;
        else if (ALU_BSel == 2'b01)
            EX_SWData = EX_MEM_ALUOut;
        else if (ALU_BSel == 2'b10)
            EX_SWData = WBData;
        else 
            EX_SWData = 32'b0;
    end

    // EX_Rd
    always @ (*) begin
        if (RSel == 2'b00)
            EX_Rd <= 5'd31;
        else if (RSel == 2'b01)
            EX_Rd <= ID_EX_rd;
        else if (RSel == 2'b10)
            EX_Rd <= ID_EX_rt;
        else
            EX_Rd <= 32'b0;
    end

    // tmp_ALU_Res
    always @ (*) begin
        if(ALUOp == 4'b0000)
            tmp_ALU_Res = {In_A[31],In_A} + {In_B[31],In_B};
        else if (ALUOp == 4'b0001)
            tmp_ALU_Res = {In_A[31],In_A} - {In_B[31],In_B};
        else if (ALUOp == 4'b0010)
            tmp_ALU_Res = In_A | In_B;
        else if (ALUOp == 4'b0011) begin
            if (ID_EX_sign && ($signed(In_A)) < ($signed(In_B)) )
                tmp_ALU_Res = 32'd1;
            else if (!ID_EX_sign && In_A < In_B)
                tmp_ALU_Res = 32'd1;
            else
                tmp_ALU_Res = 32'd0;
        end
        else if (ALUOp == 4'b0100)
            tmp_ALU_Res = In_A & In_B;
        else if (ALUOp == 4'b0101)
            tmp_ALU_Res = ~(In_A | In_B);
        else if (ALUOp == 4'b0110)
            tmp_ALU_Res = In_A ^ In_B;
        else if (ALUOp == 4'b0111)
            tmp_ALU_Res = {1'b0,In_B[15:0],16'b0};
        else if (ALUOp == 4'b1000) begin //left shift
            if (ID_EX_shift_var)
                tmp_ALU_Res = {1'b0,In_B << In_A[4:0]};
            else
                tmp_ALU_Res = {1'b0,In_B << ID_EX_shift};
        end
        else if (ALUOp == 4'b1001) begin // right logic shift
            if (ID_EX_shift_var)
                tmp_ALU_Res = {1'b0,In_B >> In_A[4:0]};
            else
                tmp_ALU_Res = {1'b0,In_B >> ID_EX_shift};
        end
        else if (ALUOp == 4'b1010) begin // right arithmetic shift
            if (ID_EX_shift_var)
                tmp_ALU_Res = {1'b0,($signed(In_B)) >>> In_A[4:0]};
            else
                tmp_ALU_Res = {1'b0,($signed(In_B)) >>> ID_EX_shift};
        end
        else 
            tmp_ALU_Res = 32'b0;
    end

    // IntegerOverflow
    always @ (tmp_ALU_Res or ID_EX_overflow) begin
        if (tmp_ALU_Res[32] != tmp_ALU_Res[31] && ID_EX_overflow)
            IntegerOverflow = 1;
        else
            IntegerOverflow = 0;
    end
    
  

    // A_md
    always @ (ID_EX_sign or In_A) begin
        if (ID_EX_sign && In_A[31])
            A_md = ~In_A + 1;
        else
            A_md = In_A;
    end

    // B_md
    always @ (ID_EX_sign or In_B) begin
        if (ID_EX_sign && In_B[31])
            B_md = ~In_B + 1;
        else
            B_md = In_B;
    end

    // A_change
    always @ (posedge clk) begin
        if (!ID_EX_start && busy)
            A_change <= A_change;
        else if (ID_EX_sign && In_A[31] && ID_EX_start)
            A_change <= 1;
        else
            A_change <= 0;
            
    end

    // B_change
    always @ (posedge clk) begin
        if (!ID_EX_start && busy)
            B_change <= B_change;
        else if (ID_EX_sign && In_B[31] && ID_EX_start)
            B_change <= 1;
        else
            B_change <= 0;
    end

    // Reserved_ALUOp
    always @ (posedge clk) begin
        if (ID_EX_start && busy && !all_flush)
            Reserved_ALUOp <= ALUOp;
        else if (!ID_EX_start && busy && !all_flush)
            Reserved_ALUOp <= Reserved_ALUOp;
        else
            Reserved_ALUOp <= 0;
    end

    mul_fsm U_mul (
        .multiplicand(B_md),.multiplier(A_md),.clock(clk),.rst(rst),
        .start(start),

        .busy(mul_busy),.product(product),.HLWr(HLWr)
    ); 

    div_fsm U_div (
        .dividend(A_md),.divisor(B_md),.start(start),.clock(clk),
        .rst(rst),

        .quotient(quotient),.remainder(remainder),.busy(div_busy),.count(div_count)
    );

    // reg_quotient
    always @ (A_change or B_change or quotient) begin
        if (A_change ^ B_change)
            reg_quotient = ~quotient + 1;
        else
            reg_quotient = quotient;
    end

    // reg_remainder
    always @ (A_change or remainder) begin
        if (A_change)
            reg_remainder = ~remainder + 1;
        else
            reg_remainder = remainder;
    end

    // reg_product
    always @ (A_change or B_change or product) begin
        if (A_change ^ B_change)
            reg_product = ~product + 1;
        else
            reg_product = product;
    end
    
    // High
    always @ (Reserved_ALUOp or reg_product or reg_remainder or ID_EX_HLOp or In_A) begin
        if (ID_EX_HLOp == 3'b010)
            High = In_A;
        else if (Reserved_ALUOp == 4'b1011)
            High = reg_product[63:32];
        else if (Reserved_ALUOp == 4'b1100)
            High = reg_remainder;
        else
            High = 0;
    end

    // Low
    always @ (Reserved_ALUOp or reg_product or reg_quotient or ID_EX_HLOp or In_A)  begin
        if (ID_EX_HLOp == 3'b011)
            Low = In_A;
        else if (Reserved_ALUOp == 4'b1011)
            Low = reg_product[31:0];
        else if (Reserved_ALUOp == 4'b1100)
            Low = reg_quotient;
        else
            Low = 0;
    end

    // busy
    always @ (mul_busy or div_busy or ALUOp) begin
        if (mul_busy || div_busy || ALUOp == 4'b1011 || ALUOp == 4'b1100)
            busy = 1;
        else
            busy = 0;
    end

    
    
    // Hi_HLWr
    always @ (HLWr or ID_EX_HLOp or all_flush) begin
        if (HLWr && !all_flush)
            Hi_HLWr = 1;
        else if (ID_EX_HLOp == 3'b010 && !all_flush)
            Hi_HLWr = 1;
        else
            Hi_HLWr = 0;
    end

    // Lo_HLWr
    always @ (HLWr or ID_EX_HLOp or all_flush) begin
        if (HLWr && !all_flush)
            Lo_HLWr = 1;
        else if (ID_EX_HLOp == 3'b011 && !all_flush)
            Lo_HLWr = 1;
        else
            Lo_HLWr = 0;
    end
    
    // start
    always @ (all_flush or ID_EX_start) begin
        if (all_flush)
            start = 0;
        else
            start = ID_EX_start;
    end

endmodule