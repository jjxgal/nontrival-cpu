// OP: 10 is signed
//     01 is unsigned
//     00 is invalid
module multiplier(
	input                 clk,
	input                 resetn,
	input  [1 :0]         mult_op,
	input  [31:0]         mult_op1,
	input  [31:0]         mult_op2,
	output [63:0]         product,
	output                mult_end
);
	reg  [31:0]   mult_op1_temp, mult_op2_temp;
	wire [63:0]   product_temp;
	reg           p_sign;
	reg  [2 :0]   counter;

	assign product    = p_sign ? -product_temp : product_temp;
	assign mult_end = ( counter == 3'b000 );

	wire            p_sign_temp = (mult_op == 2'b10) ? ( mult_op1[31] ^ mult_op2[31] ) : 1'b0;
	wire [31: 0]    op1_temp    = (mult_op == 2'b10) ? ( mult_op1[31] ? ~mult_op1 + 1 : mult_op1 ) : mult_op1;
	wire [31: 0]    op2_temp    = (mult_op == 2'b10) ? ( mult_op2[31] ? ~mult_op2 + 1 : mult_op2 ) : mult_op2;
	//counter
	always @(posedge clk) begin
		if(!resetn) begin
			counter <= 3'd0;
		end
		else begin
			if(!mult_end) begin
				counter <= counter - 1;
			end
			else begin 
				if(mult_op == 2'b01 || mult_op == 2'b10) begin
					counter <= 3'd4;
				end
			end
		end
	end
	//data
	always @( posedge clk ) begin
		if(!resetn) begin
			p_sign        <= 1'b0;
			mult_op1_temp <= 32'd0;
			mult_op2_temp <= 32'd0;
		end
		else if(mult_op != 2'b00) begin
				p_sign        <= p_sign_temp;
				mult_op1_temp <= op1_temp;
				mult_op2_temp <= op2_temp;
		end
	end
	//IP
	mult_gen_0 mult(
		.CLK    (clk),
		.A      (mult_op1_temp),
		.B      (mult_op2_temp),
		.P      (product_temp)
	);

endmodule
