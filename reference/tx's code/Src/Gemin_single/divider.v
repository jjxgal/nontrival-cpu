module divider(
	input             clk,
	input             resetn,
	input  [ 1:0]     div_op,
	input  [31:0]     divisor,
	input  [31:0]     dividend,
	output [63:0]     div_result,
	output            div_end
);

	reg  [5 :0]       counter;
	reg  [31:0]       divisor_temp , dividend_temp;
	reg               div_sign;
	reg               dividend_sign;
	wire [31:0]       remainder;
	wire [31:0]       quotient;
	wire [63:0]       div_result_temp;

	assign quotient    = div_sign      ? (~div_result_temp[63:32] + 32'd1) : div_result_temp[63:32];
	assign remainder   = dividend_sign ? (~div_result_temp[31:0 ] + 32'd1) : div_result_temp[31:0 ];
	assign div_end     = counter == 6'd0;
	assign div_result  = { remainder, quotient };

	wire        sign          = ( div_op == 2'b10 ) ? ( divisor[31] ^ dividend[31]) : 1'b0;
	wire [31:0] temp_divisor  = ( div_op == 2'b10 ) ? ( divisor[31]  ? ~divisor  + 1 : divisor  ) : divisor;
	wire [31:0] temp_dividend = ( div_op == 2'b10 ) ? ( dividend[31] ? ~dividend + 1 : dividend ) : dividend;
	wire        sign_dividend = ( div_op == 2'b10 ) ? dividend[31] : 1'b0;
	//counter
	always @(posedge clk) begin
		if(!resetn) begin
			counter <= 6'd0;
		end
		else begin
			if(!div_end)
				counter <= counter - 6'd1;
			else begin
				if(div_op == 2'b10 || div_op == 2'b01) begin
					counter <= 6'd34;
				end
			end
		end
	end
	//data
	always @( posedge clk ) begin
		if(!resetn) begin
			div_sign      <= 1'b0;
			divisor_temp  <= 32'd0;
			dividend_temp <= 32'd0;
		end
		else begin
			if(div_op != 2'b00 ) begin
				div_sign      <= sign;
				divisor_temp  <= temp_divisor;
				dividend_temp <= temp_dividend;
				dividend_sign <= sign_dividend;
			end
		end
	end

	//IP
	div_gen_0 div(
		.aclk                         (clk),
		.s_axis_divisor_tdata         (divisor_temp),
		.s_axis_divisor_tvalid        (1'd1),
		.s_axis_dividend_tdata        (dividend_temp),
		.s_axis_dividend_tvalid       (1'd1),
		.m_axis_dout_tdata            (div_result_temp)
	);

endmodule
