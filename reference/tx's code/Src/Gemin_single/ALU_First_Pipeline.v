    `include "define.v"
    module ALU_First_Pipeline(
        input wire clk,
        input wire resetn,
        input wire flush,
        input  wire  [31:0]  busa,
        input  wire  [31:0]  busb,
        input  wire  [63:0]  HiLo,
        input  wire  [6:0]  aluop,
        input  wire  [31:0] pc,
        input  wire  [13:0] Exp_First_old,//exp[1]==1'b1,if overflow;
        input  wire  [31:0] CP0_Data,
        output reg  [13:0] Exp_First_new,
        output reg  [31:0] Out,
        output reg  [63:0] WHILO_Data,
        input  mul_div_new,
        output  stall// multicycle
    );
    //Out source HILO,ALU_Result,CP0,rs,rt,
    wire [31:0] Hi;
    wire [31:0] Lo;
    wire [31:0] temp_result;
    wire [4:0] sa;
    assign sa=busa[10:6];
    assign Hi=HiLo[63:32];
    assign Lo=HiLo[31:0];

    reg             mult_end_prev, div_end_prev;
    wire            mult_end, div_end;
    wire [63:0]     mult_result, div_result;
    reg  [1:0]      mult_op, div_op;
    wire            mult_commit, div_commit;

    always@(*)begin
        case(aluop)
            `LB_OP,`LBU_OP,`LH_OP,`LHU_OP,`LW_OP,`SB_OP,`SH_OP,`SW_OP,`ADD_OP,`ADDI_OP,`ADDU_OP,`ADDIU_OP:begin
            Out=busa+busb;
            end
            `SUB_OP,`SUBU_OP:begin
            Out=busa-busb;
            end
            `SLT_OP,`SLTI_OP:begin
            Out=($signed(busa)<$signed(busb))?32'd1:32'd0;
            end
            `SLTU_OP,`SLTIU_OP:begin
            Out=(busa<busb)?32'd1:32'd0;
            end
            `AND_OP,`ANDI_OP:begin
            Out=busa&busb;
            end
            `LUI_OP:begin
            Out={busa[15:0],16'd0};
            end
            `NOR_OP:begin
            Out=~(busa|busb);
            end
            `OR_OP,`ORI_OP:begin
            Out=busa|busb;
            end
            `XOR_OP,`XORI_OP:begin
            Out=busa^busb;
            end
            `MFC0_OP:begin
            Out=CP0_Data;
            end
            `MTC0_OP:begin
            Out=busb;
            end
            `SLLV_OP:begin
            Out=busb<<busa[4:0];
            end
            `SLL_OP:begin
            Out=busb<<sa;
            end
            `SRLV_OP:begin
            Out=busb>>busa[4:0];
            end
            `SRL_OP:begin
            Out=busb>>sa;
            end
            `SRAV_OP:begin
            Out=($signed(busb))>>>(busa[4:0]);
            end
            `SRA_OP:begin
            Out=($signed(busb))>>>sa;
            end
            `MFHI_OP:begin
            Out=Hi;
            end
            `MFLO_OP:begin
            Out=Lo;
            end
            `MTHI_OP,`MTLO_OP:begin
            Out=busa;
            end
            `MUL_OP:begin
            Out=mult_result[31:0];
            end
            `BGEZAL_OP,`BLTZAL_OP,`JALR_OP,`JAL_OP:begin
            Out=pc+32'd8;
            end
            default:begin
            Out=32'h0;
            end
        endcase
    end
    //overflow
    always@(*)begin
        case(aluop)
            `ADD_OP,`ADDI_OP:begin
                if((~(busa[31] ^ busb[31])) & (Out[31] ^ busa[31]))begin
                    Exp_First_new={Exp_First_old[13:2],1'b1,Exp_First_old[0]};
                end
                else begin
                    Exp_First_new=Exp_First_old;
                end
            end
            `SUB_OP:begin
                if((busa[31]^busb[31]) & (busa[31]^Out[31]))begin
                    Exp_First_new={Exp_First_old[13:2],1'b1,Exp_First_old[0]};
                end
                else begin
                    Exp_First_new=Exp_First_old;
                end
            end
            default:begin
            Exp_First_new=Exp_First_old;
            end
        endcase
    end

    //qfs
    reg [3:0] mul_div_cnt;
    always@(posedge clk) begin
        if(mul_div_new) begin
            mul_div_cnt <= 0;
        end
        else if(div_end!= div_end_prev || mult_end != mult_end_prev) begin
            mul_div_cnt <= mul_div_cnt + 1;
        end
        else begin
            mul_div_cnt <= mul_div_cnt;
        end
    end
    wire mul_div = (mul_div_cnt != 2);

    reg             mdu_prepare;
    wire            mdu_running = (~(mult_end & div_end) || mdu_prepare ) && (mul_div_cnt !=2);

    assign stall       = flush ? 0 : mdu_running;
    assign mult_commit = mult_end && ( (mult_end_prev != mult_end) || (!mul_div)) && ( ( aluop == 7'd13 ) || (aluop == 7'd14) || (aluop==7'd59) );//是否可以提交乘法的结果
    assign div_commit  = div_end  && ( (div_end_prev  != div_end ) || (!mul_div)) && ( ( aluop == 7'd11 ) || (aluop == 7'd12) );                   //是否可以提交除法的结果

    always @ (posedge clk) begin
        if(!resetn) begin
            mult_end_prev <= 1'b0;
            div_end_prev <= 1'b0;
        end
        else begin
            mult_end_prev <= mult_end;
            div_end_prev <= div_end;
        end
    end

    //div_op和mult_op是乘除法开始的标志
    always @ ( * ) begin
        div_op = 2'b00;
        mult_op = 2'b00;
        mdu_prepare = 1'b0;
        if(!flush && (mult_end & div_end) && (mult_end_prev == mult_end) && (div_end_prev == div_end)) begin 
            mdu_prepare = 1'b1;
            case(aluop)
                `DIV_OP: begin
                    div_op = 2'b10;
                    //div_start = 1'b1;
                end
                `DIVU_OP: begin
                    div_op = 2'b01;
                    //div_start = 1'b1;
                end
                `MULT_OP,`MUL_OP: begin
                    mult_op = 2'b10;
                    //mul_start = 1'b1;
                end
                `MULTU_OP: begin
                    mult_op = 2'b01;
                    //mul_start = 1'b1;
                end
                default: begin
                    mdu_prepare = 1'b0;
                end
            endcase
        end
        else begin
            mdu_prepare = 1'b0;
        end
    end

    divider div(
        .clk        (clk),
        .resetn     (resetn),
        .div_op     (div_op & {2{mul_div}}),
        .divisor    (busb),
        .dividend   (busa),
        .div_result (div_result),
        .div_end    (div_end)
    );

    multiplier mult(
        .clk        (clk),
        .resetn     (resetn),
        .mult_op    (mult_op & {2{mul_div}}),
        .mult_op1   (busa),
        .mult_op2   (busb),
        .product    (mult_result),
        .mult_end   (mult_end)
    );

    always @ ( * ) begin
        WHILO_Data = 64'd0;
        if(div_commit)
            WHILO_Data = div_result;
        else if(mult_commit) begin
                WHILO_Data = mult_result;
            end
        else begin
            case(aluop)
            `MTHI_OP:begin
            WHILO_Data={busa,Lo};
            end
            `MTLO_OP:begin
            WHILO_Data={Hi,busa};
            end
            endcase
        end
    end
endmodule