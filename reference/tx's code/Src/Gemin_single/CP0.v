`include "define.v"
module CP0(
    input wire clk,
    input wire resetn,
    input wire [5:0] int_,
    input wire has_exp,//from exception
    input exp_first,
    input exp_second,
    input wire [7:0] cp0_addr,
    input wire [7:0] cp0_write_addr,
    input wire write_cp0_enable,
    input wire [31:0] write_cp0_data,
    input wire exp_is_in_delayslot,
    input wire wen_badaddress,
    input wire wen_epc,
    input wire wen_causebd,
    input wire [31:0] exp_badaddress,
    input wire [31:0] exp_epc,
    input wire [4:0] exp_cause_code,
    input wire exp_clean,
    input wire tlbr,
    input wire tlbp,
    input wire [85:0] tlb_entry_in,
    output wire [85:0] tlb_entry_out,
    input wire probe_fail,//from  tlb
    input wire [3:0] probe_index,//from tlb
    output wire [3:0] cp0_index,//to tlb
    output wire [3:0] cp0_random,//to tlb
    output wire [31:0] epc_to_Exception,
    output reg [31:0] cp0_data,
    output wire is_in_exp,
    output wire [1:0]allow_int,
    output wire interrupt_flag,
    output wire [7:0] cp0_asid,//new
    output wire kesg0_uncached,  //new
    output wire user_mode      //new
);
reg [31:0]Index;
reg [31:0]Random;
reg [31:0]EntryLo0;
reg [31:0]EntryLo1;
reg [31:0]Context;
reg [31:0]PageMask;
reg [31:0]Config;
reg [31:0]Cause;
reg [31:0]BadVaddr;
reg [32:0]Count;
reg [31:0]Status;
reg [31:0]EntryHi;
reg [31:0]Compare;
reg [31:0]Epc;
reg [31:0]Ebase;
//now stage is handling a previous exception
//if is_in_exp==1 and another exception is happenging,never write the epc,but cause and badvadder
assign is_in_exp=(Status[1]==1'b1);
assign epc_to_Exception=Epc;
assign allow_int=Status[1:0];//(Status[1:0]==2'b01);
assign user_mode=(Status[4:1]==4'b1000);
assign kesg0_uncached=(Config[2:0] == 3'd2);
assign interrupt_flag=((Status[15]&Cause[15])||(Status[14]&Cause[14])||
                        (Status[13]&Cause[13])||(Status[12]&Cause[12])||
                        (Status[11]&Cause[11])||(Status[10]&Cause[10]));//(Status[15:8]&Cause[15:8]);
assign cp0_asid=EntryHi[7:0];
always@(posedge clk)begin
    if(!resetn)begin
        Count<= 33'd0;
        Status<= 32'h1040_0004;
        Cause<= 32'd0;
        BadVaddr<=32'd0;
        EntryHi<= 32'd0;
        Epc<=32'd0;
        EntryLo0[31:30] <= 2'd0;
        EntryLo1[31:30] <= 2'd0;
        Ebase<= 32'h80000000;
        Index<= 32'd0;
        Context<= 32'd0;
        Compare<= 32'd0;
        Config<={1'b1,21'd0,3'b1,7'd0};
        Random<= 32'd0;
    end
    else begin
        //Write CP0
        Count <= Count + 33'd1;
        Cause[15:10]<=int_;//qfs
        Random<=Count[4:1];
        if(write_cp0_enable)begin
            //$display(" wb stage is writing cp0,addr=%8b,Write_data is 0x%8h,Status is 0x%8h",cp0_write_addr,write_cp0_data,Status);
            case(cp0_write_addr)
            `Count_Addr:begin
                Count<={write_cp0_data, 1'b0};
            end
            `Cause_Addr:begin
                Cause[9:8]<= write_cp0_data[9:8];
                //Cause[23]<= write_cp0_data[23];
            end
            `Status_Addr:begin
                Status[28]<= write_cp0_data[28];
                Status[22]<= write_cp0_data[22];
                Status[15:8]<= write_cp0_data[15:8];
                Status[4]<= write_cp0_data[4];
                Status[2:0]<= write_cp0_data[2:0];//changed
            end
            `Epc_Addr:begin
                Epc<=write_cp0_data;
            end
            `Context_Addr:begin
                Context[31:13]<= write_cp0_data[31:13];
            end
            `Ebase_Addr:begin
                Ebase[29:12]<=write_cp0_data[29:12];
                Ebase[9:0]<=write_cp0_data[9:0];
            end
            `Index_Addr:begin
                Index<=write_cp0_data;
            end
            default:begin
              
            end
            endcase
        end
        //Exp Relative Write 
        if(has_exp&&exp_clean==1'b0) begin
            if(wen_badaddress==1'b1)begin
                BadVaddr<=exp_badaddress;
            end
             if(wen_epc)begin Epc<=exp_epc;end
            Cause[6:2]<=exp_cause_code;
             if(wen_causebd) begin Cause[31]<=exp_is_in_delayslot;end
            Status[1]<=~exp_clean; 
            //if(exp_clean==1'b1)begin
        //    Status[1]<=1'b0;
        end
        if(exp_clean==1'b1)begin
            Status[1]<=1'b0;
        end
    end
end
always@(posedge clk)begin
    //$display("ex is reading cp0 addr=%7b,cp0_data=0x%8h",cp0_addr,cp0_data);
end
always@(*)begin
    case(cp0_addr)
        `Count_Addr:begin
            cp0_data=Count[32:1];
        end
        `Index_Addr:begin
            cp0_data=Index;
        end
        `BadVaddr_Addr:begin
            if(write_cp0_enable==1'b1&&cp0_write_addr==`BadVaddr_Addr)begin
                cp0_data=write_cp0_data;
            end
            else begin
                cp0_data=BadVaddr;
            end
        end
        `Cause_Addr:begin
           if(write_cp0_enable==1'b1&&cp0_write_addr==`Cause_Addr)begin
               cp0_data={Cause[31:10],write_cp0_data[9:8],Cause[7:0]};
            end
            else begin
                cp0_data=Cause;
            end 
        end
         `Ebase_Addr:begin
            if(write_cp0_enable==1'b1&&cp0_write_addr==`Ebase_Addr)begin
                cp0_data={Ebase[31:30],write_cp0_data[29:12],Ebase[11:10],write_cp0_data[9:0]};
            end
            else begin
                cp0_data=Ebase;
            end
        end
        `Epc_Addr:begin
            if(write_cp0_enable==1'b1&&cp0_write_addr==`Epc_Addr)begin
                cp0_data=write_cp0_data;
            end
            else begin
                cp0_data=Epc;
            end
        end
        `Status_Addr:begin
           if(write_cp0_enable==1'b1&&cp0_write_addr==`Status_Addr)begin
                cp0_data={Status[31:29],write_cp0_data[28],Status[27:23],write_cp0_data[22],Status[21:16],write_cp0_data[15:8],Status[7:5],write_cp0_data[4],Status[3],write_cp0_data[2:0]};
            end 
            else begin
                cp0_data=Status;
            end 
        end
        default:begin
            cp0_data=32'h0;
        end
    endcase
end

endmodule


